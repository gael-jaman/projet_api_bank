INSERT INTO Role (id, name)
VALUES ('1', 'ROLE_ADMIN');

INSERT INTO Role (id, name)
VALUES ('2', 'ROLE_USER');

INSERT INTO user (id, first_name, last_name, email, no_passport, num_phone, password, birthday_date)
VALUES ('1', 'Gael', 'Jaman', 'gael@hotmail.com', 'ABCDEFGHIJK123', '0664417517', '$argon2id$v=19$m=4096,t=3,p=1$fefBgmM1y/uwuHUPLE3QgQ$6ykG6s2XoT5xIeJU+92AX2CGPw+E3WdO5pVqYSRTd5E', '20/05/1999');

INSERT INTO user (id, first_name, last_name, email, no_passport, num_phone, password, birthday_date)
VALUES ('2', 'Elouan', 'Bristiel', 'elou@gmail.com', 'ABCDEFGHIJK124', '0678641728', '$argon2id$v=19$m=4096,t=3,p=1$fefBgmM1y/uwuHUPLE3QgQ$6ykG6s2XoT5xIeJU+92AX2CGPw+E3WdO5pVqYSRTd5E', '17/06/1999');

INSERT INTO User_Roles (user_id, roles_id)
VALUES('1', '1');

INSERT INTO User_Roles (user_id, roles_id)
VALUES('2', '2');

INSERT INTO Account(iban, balance, country, user_id)
VALUES ('FR7612548029989876543210917', 400, 'France', '1');

INSERT INTO Account(iban, balance, country, user_id)
VALUES ('FR7612548029989876543210910', 400, 'Allemagne', '1');

INSERT INTO Account(iban, balance, country, user_id)
VALUES ('FR7612548029989876543287145', 400, 'France', '2');

INSERT INTO Account(iban, balance, country, user_id)
VALUES ('FR7612548029989876543282146', 400, 'Canada', '2');

INSERT INTO Card(num_card, secret_code, visual_cryptogram, expiration_date, blocked, localisation, ceiling, without_contact, is_virtual_card, account_iban, deleted_date)
VALUES ('4012001037141112', '1234', '123', '01/08/2025', false, true, 1500, true, false, 'FR7612548029989876543210917', '');

INSERT INTO Card(num_card, secret_code, visual_cryptogram, expiration_date, blocked, localisation, ceiling, without_contact, is_virtual_card, account_iban, deleted_date)
VALUES ('4012001038443335', '4321', '321', '01/02/2022', false, true, 1500, true, true, 'FR7612548029989876543210917', '');

INSERT INTO Card(num_card, secret_code, visual_cryptogram, expiration_date, blocked, localisation, ceiling, without_contact, is_virtual_card, account_iban, deleted_date)
VALUES ('4012001037141113', '1234', '123', '01/08/2025', false, true, 1500, true, false, 'FR7612548029989876543287145', '');

INSERT INTO Transfer(id, date, libelle, amount, rate_apply, debtor_account_iban, target_account_iban)
VALUES ('1', '01/02/2022', 'Virement de 20€', 20, 1, 'FR7612548029989876543210917', 'FR7612548029989876543287145');

INSERT INTO Transfer(id, date, libelle, amount, rate_apply, debtor_account_iban, target_account_iban)
VALUES ('2', '01/02/2022', 'Virement de 100 USD canadien', 100, 0.78, 'FR7612548029989876543282146', 'FR7612548029989876543210917');

INSERT INTO Transaction(id, date, libelle, amount, rate_apply, card_number, receiver_account_iban)
VALUES ('1', '01/02/2022', 'Paiement de 50€', 50, 1, '4012001037141112', 'FR7612548029989876543287145');

INSERT INTO Transaction(id, date, libelle, amount, rate_apply, card_number, receiver_account_iban)
VALUES ('2', '01/02/2022', 'Paiement de 50€', 50, 1, '4012001037141113', 'FR7612548029989876543210917');
