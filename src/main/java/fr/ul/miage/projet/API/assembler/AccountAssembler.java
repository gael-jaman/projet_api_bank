package fr.ul.miage.projet.API.assembler;

import fr.ul.miage.projet.API.controller.AccountController;
import fr.ul.miage.projet.API.entity.Account;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.springframework.hateoas.server.core.DummyInvocationUtils.methodOn;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

@Component
public class AccountAssembler implements RepresentationModelAssembler<Account, EntityModel<Account>> {
    @Override
    public EntityModel<Account> toModel(Account acount) {
        return EntityModel.of(acount,
                linkTo(methodOn(AccountController.class).getAccountByIBAN(acount.getIban())).withSelfRel(),
                linkTo(methodOn(AccountController.class).getAllAccounts()).withRel("collection"));
    }

    public CollectionModel<EntityModel<Account>> toCollectionModel(Iterable<? extends Account> entities) {
        List<EntityModel<Account>> accountModel = StreamSupport
                .stream(entities.spliterator(), false)
                .map(i -> toModel(i))
                .collect(Collectors.toList());
        return CollectionModel.of(accountModel,
                linkTo(methodOn(AccountController.class)
                        .getAllAccounts()).withSelfRel());
    }
}
