package fr.ul.miage.projet.API.serviceImplementation;

import fr.ul.miage.projet.API.entity.Card;
import fr.ul.miage.projet.API.repository.CardRepository;
import fr.ul.miage.projet.API.service.CardService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Transactional
@Slf4j
public class CardServiceImplement implements CardService {

    private final CardRepository cardRepo;

    @Override
    public Card saveCard(Card card) {
        log.info("Sauvegarde d'une nouvelle carte pour le compte: " + card.getAccount().getIban() + " de monsieur: " + card.getAccount().getOwner().getEmail());
        return cardRepo.save(card);
    }

    @Override
    public Optional<Card> getByCardNumber(String cardNumber) {
        log.info("On récupère la carte ayant l'id suivant : " + cardNumber);
        return cardRepo.findById(cardNumber);
    }

    @Override
    public Optional<Card> getByCardNumberAndCCVAndExpirationDate(String cardNumber, String CCV, String exprirationDate) {
        Optional<Card> card = getByCardNumber(cardNumber);

        if (card.isPresent()) {
            if (card.get().getVisual_cryptogram().equals(CCV)) {
                if (card.get().getExpiration_date().equals(exprirationDate)) {
                    return card;
                }
            }
        }

        return null;
    }

    @Override
    public List<Card> getCardsByAccountIban(String iban) {
        log.info("On récupère toutes les cartes du compte suivant : " + iban);
        List<Card> listCards = cardRepo.findAll();

        ArrayList<Card> cardsToShow = new ArrayList<Card>();
        for (Card card : listCards) {
            if (card.getAccount().getIban().equals(iban)) {
                cardsToShow.add(card);
            }
        }

        return cardsToShow;
    }

    @Override
    public List<Card> getAllCards() {
        log.info("On récupère toutes les cartes");
        return cardRepo.findAll();
    }

    @Override
    public Card lockCard(Card card) {
        log.info("On verouille la carte");
        card.setBlocked(true);
        return card;
    }

    @Override
    public Card unlockCard(Card card) {
        log.info("On déverouille la carte");
        card.setBlocked(false);
        return card;
    }

    @Override
    public Card enableLocalication(Card card) {
        log.info("On déverouille la lcoalication");
        card.setLocalisation(true);
        return card;
    }

    @Override
    public Card disableLocalication(Card card) {
        log.info("On vérouille la lcoalication");
        card.setLocalisation(false);
        return card;
    }

    @Override
    public Card enableWithoutContact(Card card) {
        log.info("On active le sans contact");
        card.setWithout_contact(true);
        return card;
    }

    @Override
    public Card disableWithoutContact(Card card) {
        log.info("On décative le sanc contact");
        card.setWithout_contact(false);
        return card;
    }

    @Override
    public void deleteCard(Card card) {
        log.info("On supprime la carte");
        // On récupère la date current
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        Date date = new Date();
        String currentDate = formatter.format(date).toString();

        card.setDeleted_date(currentDate);
        cardRepo.save(card);
    }

    @Override
    public Boolean isAlreadyValidCard(Card card) throws ParseException {

        // Date courrante
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        Date date = new Date();
        String dateFormat = formatter.format(date).toString();

        // On compare 2 dates
        Date date1 = formatter.parse(dateFormat); // Current date
        Date date2 = formatter.parse(card.getExpiration_date()); // Card date

        int result = date1.compareTo(date2);
        System.out.println("result: " + result);

        Boolean isValid = true;
        if (result == 0 || result > 0) { // Carte expirée
            isValid = false;
        }

        return isValid;
    }
}
