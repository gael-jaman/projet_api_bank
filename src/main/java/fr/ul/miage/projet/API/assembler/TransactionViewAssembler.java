package fr.ul.miage.projet.API.assembler;

import fr.ul.miage.projet.API.entity.Transaction;
import fr.ul.miage.projet.API.views.TransactionView;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

@Component
public class TransactionViewAssembler implements RepresentationModelAssembler<Transaction, TransactionView> {
    @Override
    public TransactionView toModel(Transaction transaction) {
        TransactionView transactionView = new TransactionView(transaction);
        return  transactionView;
    }
}
