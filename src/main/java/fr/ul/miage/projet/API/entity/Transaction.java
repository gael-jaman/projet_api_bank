package fr.ul.miage.projet.API.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Data
@NoArgsConstructor
public class Transaction implements Serializable { // Cette entité représente les opérations effectués à l'aide d'une carte bancaire

    private static final long serialVersionUID = 1034298358L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private String id;
    private String date;
    private String libelle;
    private Double amount;
    private Double rateApply;
    @ManyToOne
    @JoinColumn(name = "card_number")
    private Card card;
    @ManyToOne
    @JoinColumn(name = "receiver_account_iban")
    private Account receiverAccount;

    public Transaction(String id, String date, String libelle, Double amount, Double rateApply, Card card, Account receiverAccount) {
        this.id = id;
        this.date = date;
        this.libelle = libelle;
        this.amount = amount;
        this.rateApply = rateApply;
        this.card = card;
        this.receiverAccount = receiverAccount;
    }
}
