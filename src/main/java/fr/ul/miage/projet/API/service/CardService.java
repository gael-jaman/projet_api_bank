package fr.ul.miage.projet.API.service;

import fr.ul.miage.projet.API.entity.Account;
import fr.ul.miage.projet.API.entity.Card;

import java.text.ParseException;
import java.util.List;
import java.util.Optional;

public interface CardService {

    // Permet de sauvegarder une carte
    Card saveCard(Card card);

    // Permet de récupérer la carte à partir de son numéro de carte
    Optional<Card> getByCardNumber(String cardNumber);

    Optional<Card> getByCardNumberAndCCVAndExpirationDate(String cardNumber, String CCV, String exprirationDate);

    // Permet de récupérer une carte grâce à l'iban du compte
    List<Card> getCardsByAccountIban(String iban);

    // Permet de récupérer tous les utilisateurs
    List<Card> getAllCards();

    // Permet de vérouiller une carte
    Card lockCard(Card card);

    // Permet de déverouiller une carte
    Card unlockCard(Card card);

    // Active le fait de payer avec une carte à l'étranger
    Card enableLocalication(Card card);

    // Désactive le fait de payer avec une carte à l'étranger
    Card disableLocalication(Card card);

    // Active le sans contact
    Card enableWithoutContact(Card card);

    // Désactive le sans contact
    Card disableWithoutContact(Card card);

    // Permet de supprimer une carte
    void deleteCard(Card card);

    // Permet de vérifier si la date d'une carte est toujours valide
    Boolean isAlreadyValidCard(Card card) throws ParseException;



}
