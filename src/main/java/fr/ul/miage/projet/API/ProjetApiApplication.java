package fr.ul.miage.projet.API;

import fr.ul.miage.projet.API.inputFormat.CountryDeviseAndIBANkeyFormat;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.argon2.Argon2PasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@SpringBootApplication
public class ProjetApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjetApiApplication.class, args);
	}

	@Bean
	PasswordEncoder passwordEncoder(){
		return new Argon2PasswordEncoder();
	}

	@Bean
	Argon2PasswordEncoder argon2PasswordEncoder(){
		return new Argon2PasswordEncoder();
	}

	public static String generateCode(int codeLength) {

		// On définit les paramètres pour la génération des codes
		int max = 9;
		int min = 1;
		int range = max - min + 1;

		String result = "";
		for (int i = 0; i < codeLength; i++) {
			int rand = (int)(Math.random() * range) + min;
			result += String.valueOf(rand);
		}

		return result;
	}

}
