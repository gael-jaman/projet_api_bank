package fr.ul.miage.projet.API.assembler;

import fr.ul.miage.projet.API.entity.Transfer;
import fr.ul.miage.projet.API.views.TransferView;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

@Component
public class TransferViewAssembler implements RepresentationModelAssembler<Transfer, TransferView> {
    @Override
    public TransferView toModel(Transfer transfer) {
        TransferView transferView = new TransferView(transfer);
        return  transferView;
    }
}
