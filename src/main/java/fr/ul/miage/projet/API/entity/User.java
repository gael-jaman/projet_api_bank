package fr.ul.miage.projet.API.entity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class User implements Serializable {

    private static final long serialVersionUID = 1725963483L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private String id;
    private String first_name;
    private String last_name;
    private String email;
    private String no_passport;
    private String num_phone;
    private String password;
    private String birthday_date;

    // Lorsqu'on load les users on veut également loader les différents rôles
    @ManyToMany(fetch = FetchType.EAGER)
    private Collection<Role> roles = new ArrayList<>();

    public User(String id, String first_name, String last_name, String email, String no_passport, String num_phone, String password, String birthday_date) {
        this.id = id;
        this.first_name = first_name;
        this.last_name = last_name;
        this.email = email;
        this.no_passport = no_passport;
        this.num_phone = num_phone;
        this.password = password;
        this.birthday_date = birthday_date;
    }
}
