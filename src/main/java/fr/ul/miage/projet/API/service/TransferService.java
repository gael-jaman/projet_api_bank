package fr.ul.miage.projet.API.service;

import fr.ul.miage.projet.API.entity.Transfer;

import java.util.List;
import java.util.Optional;

public interface TransferService {

    // Permet de sauvegarder un virement
    Transfer saveTransfer(Transfer transfer);

    // Permet de récupérer le virement à partir de son id
    Optional<Transfer> getById(String id);

    // Permet de récupérer les virements à partir de l'iban d'un compte
    List<Transfer> getTransfersByAccountIban(String iban);

    // Permet de récupérer tous les virements
    List<Transfer> getAllTransfers();
}
