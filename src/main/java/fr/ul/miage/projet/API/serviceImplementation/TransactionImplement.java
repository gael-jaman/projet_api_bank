package fr.ul.miage.projet.API.serviceImplementation;

import fr.ul.miage.projet.API.entity.Transaction;
import fr.ul.miage.projet.API.entity.Transfer;
import fr.ul.miage.projet.API.repository.TransactionRepository;
import fr.ul.miage.projet.API.repository.TransferRepository;
import fr.ul.miage.projet.API.service.TransactionService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Transactional
@Slf4j
public class TransactionImplement implements TransactionService {

    private final TransactionRepository transactionRepo;

    @Override
    public Transaction saveTransaction(Transaction transaction) {
        log.info("Sauvegarde d'une nouvelle transaction ayant l'id suivant : " + transaction.getId());
        return transactionRepo.save(transaction);
    }

    @Override
    public Optional<Transaction> getById(String id) {
        log.info("On récupère la transaction ayant l'id suivant : " + id);
        return transactionRepo.findById(id);
    }

    @Override
    public List<Transaction> getTransactionsByCardNumber(String cardNumber) {
        log.info("On récupère toutes les transactions correspondant à la carte suivante : " + cardNumber);
        List<Transaction> listTransactions = transactionRepo.findAll();

        ArrayList<Transaction> transactionsToShow = new ArrayList<Transaction>();
        for (Transaction transaction : listTransactions) {
            if (transaction.getCard().getNum_card().equals(cardNumber)) {
                transactionsToShow.add(transaction);
            }
        }

        return transactionsToShow;
    }

    @Override
    public List<Transaction> getTransactionsByAccountIban(String iban) {
        log.info("On récupère toutes les transactions ayant été fais au compte suivant : " + iban);
        List<Transaction> listTransactions = transactionRepo.findAll();

        ArrayList<Transaction> transactionsToShow = new ArrayList<Transaction>();
        for (Transaction transaction : listTransactions) {
            if (transaction.getReceiverAccount().getIban().equals(iban)) {
                transactionsToShow.add(transaction);
            }
        }

        return transactionsToShow;
    }

    @Override
    public List<Transaction> getAllTransactions() {
        log.info("On récupère toutes les transactions");
        return transactionRepo.findAll();
    }
}
