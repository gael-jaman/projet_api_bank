package fr.ul.miage.projet.API.repository;

import fr.ul.miage.projet.API.entity.Transfer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TransferRepository extends JpaRepository<Transfer, String> {
    Optional<Transfer> findById(String id);
}
