package fr.ul.miage.projet.API.Error;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class ErrorRequest {

    public static ResponseEntity<?> errorRequestMessage(String message) {
        Map<String,String> response = new HashMap<String, String>();
        response.put("message", message);
        response.put("statusCode", HttpStatus.BAD_REQUEST.toString());
        response.put("timestamp", new Date().toString());
        return ResponseEntity.badRequest().body(response);
    }
}
