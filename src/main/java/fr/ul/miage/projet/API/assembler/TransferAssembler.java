package fr.ul.miage.projet.API.assembler;

import fr.ul.miage.projet.API.controller.OperationController;
import fr.ul.miage.projet.API.entity.Transfer;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.springframework.hateoas.server.core.DummyInvocationUtils.methodOn;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

@Component
public class TransferAssembler implements RepresentationModelAssembler<Transfer, EntityModel<Transfer>> {

    @Override
    public EntityModel<Transfer> toModel(Transfer transfer) {
        return EntityModel.of(transfer,
                linkTo(methodOn(OperationController.class).getTransferById(transfer.getId())).withSelfRel(),
                linkTo(methodOn(OperationController.class).getAllTransfers()).withRel("collection"));
    }

    public CollectionModel<EntityModel<Transfer>> toCollectionModel(Iterable<? extends Transfer> entities) {
        List<EntityModel<Transfer>> transferModel = StreamSupport
                .stream(entities.spliterator(), false)
                .map(i -> toModel(i))
                .collect(Collectors.toList());
        return CollectionModel.of(transferModel,
                linkTo(methodOn(OperationController.class)
                        .getAllTransfers()).withSelfRel());
    }
}
