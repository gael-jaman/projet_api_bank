package fr.ul.miage.projet.API.inputFormat;

import lombok.*;

import javax.validation.constraints.NotNull;

@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DepositAndWithdrawInputFormat {

    @NotNull
    private Double amount;
}
