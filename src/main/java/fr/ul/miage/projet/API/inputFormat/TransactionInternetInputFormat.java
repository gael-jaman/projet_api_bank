package fr.ul.miage.projet.API.inputFormat;

import lombok.*;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TransactionInternetInputFormat {

    @NotNull
    private String libelle;

    @NotNull
    private Double amount;

    @NotNull
    @Size(min = 16, max = 16)
    private String num_card;

    @NotNull
    @Size(min = 3, max = 3)
    private String ccv_code;

    @NotNull
    @Pattern(regexp = "^(0[1-9]|[12][0-9]|3[01])[/](0[1-9]|1[012])[/](19|20)\\d\\d$", message="Merci de respecter le format de date suivants: 'dd/MM/yyyy'")
    private String expiration_date;

    @NotNull
    @Size(min = 27, max = 27)
    private String receiver_account_iban;
}
