package fr.ul.miage.projet.API.inputFormat;

import lombok.*;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TransferInputFormat {

    @NotNull
    private String libelle;

    @NotNull
    private Double amount;

    @NotNull
    @Size(min = 27, max = 27)
    private String debtor_iban;

    @NotNull
    @Size(min = 27, max = 27)
    private String target_iban;
}
