package fr.ul.miage.projet.API.views;

import fr.ul.miage.projet.API.entity.Transaction;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.hateoas.RepresentationModel;

@Data
@Getter
@Setter
public class TransactionView extends RepresentationModel<TransactionView> {

    private String id;

    private String date;

    private String libelle;

    private Double amount;

    private Double rateApply;

    private String cardNumber;

    private String receiver_account_iban;

    public TransactionView(Transaction transaction) {
        this.id = transaction.getId();
        this.date = transaction.getDate();
        this.libelle = transaction.getLibelle();
        this.amount = transaction.getAmount();
        this.rateApply = transaction.getRateApply();
        this.cardNumber = transaction.getCard().getNum_card();
        this.receiver_account_iban = transaction.getReceiverAccount().getIban();
    }
}
