package fr.ul.miage.projet.API.service;

import fr.ul.miage.projet.API.entity.Account;
import fr.ul.miage.projet.API.entity.User;

import java.util.List;
import java.util.Optional;

public interface AccountService {
    // Permet de sauvegarder un account
    Account saveAccount(Account account);

    // Permet de récupérer un compte en fonction de son iban
    Optional<Account> getAccountByIban(String iban);

    // Permet de récupérer un compte en fonction de son iban
    List<Account> getAccountsByUserId(String userId);

    // Permet de récupérer tous les utilisateurs
    List<Account> getAllAccounts();

    // Permet de savoir si le compte possède suffisament d'argent pour effectuer l'opération
    boolean hasEnoughAmount(Account account, Double amount);

    // Permet de retirer de l'argent à un compte
    Account decreaseBalance(Account account, Double amount);

    // Permet d'ajouter de l'argent à un compte
    Account increaseBalance(Account account, Double amount);
}
