package fr.ul.miage.projet.API.inputFormat;

import lombok.*;

import javax.validation.constraints.NotNull;

@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AccountInputFormat {

    @NotNull
    private String country; // Pays dans lequel le compte est localisé
}
