package fr.ul.miage.projet.API.inputFormat;

import lombok.*;

import javax.validation.constraints.NotNull;

@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CardInputFormat {

    @NotNull
    private Boolean localisation; // on définit si la carte peut ou non effectuer des opérations en dehors du périmètre compatible avec la localisation du téléphone

    @NotNull
    private Boolean is_virtual_card; // L'utilisateur peut créer une carte virtuel pour effectuer des paiements sur internet
}
