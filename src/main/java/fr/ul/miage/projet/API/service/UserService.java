package fr.ul.miage.projet.API.service;

import fr.ul.miage.projet.API.entity.User;
import fr.ul.miage.projet.API.entity.Role;

import java.util.List;
import java.util.Optional;

public interface UserService {

    // Permet de sauvegarder un utilisateur
    User saveUser(User user);

    // Permet de sauvegarder un rôle
    Role saveRole(Role role);

    // Permet d'ajouter un rôle à un utilisateur
    void addRoleToUser(String roleName, String userEmail);

    // Permet de récupérer un utilisateur en fonction de son email
    Optional<User> getUserByEmail(String userEmail);

    // Permet de récupérer un utilisateur en fonction de son id
    Optional<User> getUserById(String userId);

    // Permet de récupérer tous les utilisateurs
    List<User> getAllUsers();

    // Pemet de vérifier si l'utilisateur possède un role spécifique
    Boolean hasRole(User user, String role);
}
