package fr.ul.miage.projet.API.inputFormat;

import lombok.*;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TransactionTPEInputFormat {

    @NotNull
    private String libelle;

    @NotNull
    private Double amount;

    @NotNull
    private Boolean is_without_contact_paiement;

    @NotNull
    @Size(min = 16, max = 16)
    private String num_card;

    @NotNull
    @Size(min = 4, max = 4)
    private String secret_code;

    @NotNull
    @Size(min = 27, max = 27)
    private String receiver_account_iban;

}
