package fr.ul.miage.projet.API.controller;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.ul.miage.projet.API.assembler.UserAssembler;
import fr.ul.miage.projet.API.entity.User;
import fr.ul.miage.projet.API.entity.Role;
import fr.ul.miage.projet.API.inputFormat.UserInputFormat;
import fr.ul.miage.projet.API.inputFormat.AddRoleToUserInputFormat;
import fr.ul.miage.projet.API.service.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.MediaType;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.net.URI;
import java.util.*;
import java.util.stream.Collectors;

import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(value="/users", produces = MediaType.APPLICATION_JSON_VALUE)
public class UserController {

    private final UserAssembler userAss;
    private final UserService userServ;

    private PasswordEncoder passwordEncoder;

    public UserController(UserAssembler userAss, UserService userServ, PasswordEncoder passwordEncoder) {
        this.userAss = userAss;
        this.userServ = userServ;
        this.passwordEncoder = passwordEncoder;
    }

    // Permet de récupérer tous les users
    @GetMapping()
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<?> getAllUsers() {
        //System.out.println(SecurityContextHolder.getContext().getAuthentication().getName());
        return ResponseEntity.ok(userAss.toCollectionModel(userServ.getAllUsers()));
    }

    // Permet de récupérer un user en fonction de son id
    @GetMapping(value="/{userId}")
    @PreAuthorize("(hasRole('ROLE_USER') && hasPermission(#userId, 'User', 'MANAGE_USER')) || hasRole('ROLE_ADMIN')")
    public ResponseEntity<?> getUserById(@PathVariable("userId") String userId) {
        return Optional.ofNullable(userServ.getUserById(userId)).filter(Optional::isPresent)
                .map(i -> ResponseEntity.ok(userAss.toModel(i.get())))
                .orElse(ResponseEntity.notFound().build());
    }

    // Permet de créer un user
    @PostMapping("/save")
    public ResponseEntity<?> saveUser(@RequestBody @Valid UserInputFormat userVerified) throws Exception {
        // On vérifie si l'email n'est pad déjà utilisé
        if (!userServ.getUserByEmail(userVerified.getEmail()).isPresent()) {
            User newUser = userServ.saveUser(new User(UUID.randomUUID().toString(), userVerified.getFirst_name(), userVerified.getLast_name(), userVerified.getEmail(), userVerified.getNo_passport(), userVerified.getNum_phone(), passwordEncoder.encode(userVerified.getPassword()), userVerified.getBirthday_date()));
            userServ.addRoleToUser("ROLE_USER", newUser.getEmail());
            userServ.saveUser(newUser);

            URI uri = URI.create(ServletUriComponentsBuilder.fromCurrentContextPath().path("/users/save").toUriString());
            return ResponseEntity.created(uri).body(userServ.saveUser(newUser));
        } else {
            throw new Exception("Cet email est déjà utilisé");
        }
    }

    // Permet de créer un rôle
    @PostMapping("/saveRole")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<?> saveRole(@RequestBody Role role) {
        Role newRole = userServ.saveRole(new Role(role.getId(), role.getName()));

        URI uri = URI.create(ServletUriComponentsBuilder.fromCurrentContextPath().path("/users/saveRole").toUriString());
        return ResponseEntity.created(uri).body(userServ.saveRole(newRole));
    }

    // Permet d'ajouter un role à un user
    @PostMapping("/addRoleToUser")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<?> addRoleToUser(@RequestBody @Valid AddRoleToUserInputFormat form) {
        System.out.println("Ajout d'un role à un user");
        System.out.println(form.getRoleName());
        System.out.println(form.getUserEmail());
        userServ.addRoleToUser(form.getRoleName(), form.getUserEmail());
        return ResponseEntity.ok().build();
    }

    // Permet de regénérer un token à partir du refresh token
    @GetMapping("/refreshToken")
    public void refreshToken(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String authorizationHeader = request.getHeader(AUTHORIZATION);
        if (authorizationHeader != null && authorizationHeader.startsWith("Bearer ")) {
            try {
                String refreshToken = authorizationHeader.substring("Bearer ".length());
                Algorithm algorithm = Algorithm.HMAC256("secret".getBytes());
                JWTVerifier verifier = JWT.require(algorithm).build();
                DecodedJWT decodedJWT = verifier.verify(refreshToken);
                String username = decodedJWT.getSubject();
                User user = userServ.getUserByEmail(username).get();

                // On créer le nouvel access token
                String accessToken = JWT.create()
                        .withSubject(user.getEmail())
                        .withExpiresAt(new Date(System.currentTimeMillis() + 10 * 60 * 1000)) // Ici on crée un token qui sera valide 10 min
                        .withIssuer(request.getRequestURL().toString())
                        .withClaim("roles", user.getRoles().stream().map(Role::getName).collect(Collectors.toList()))
                        .sign(algorithm);

                Map<String, String> tokens = new HashMap<>();
                tokens.put("access_token", accessToken);
                response.setContentType(APPLICATION_JSON_VALUE);
                new ObjectMapper().writeValue(response.getOutputStream(), tokens);

            } catch(Exception e) {
                response.setStatus(FORBIDDEN.value());
                Map<String, String> error = new HashMap<>();
                error.put("Problème génération nouveau token: ", e.getMessage());
                response.setContentType(APPLICATION_JSON_VALUE);
                new ObjectMapper().writeValue(response.getOutputStream(), error);
            }
        } else {
            throw new RuntimeException("Le refresh token est manquant");
        }
    }
}