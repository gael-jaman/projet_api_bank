package fr.ul.miage.projet.API.controller;

import fr.ul.miage.projet.API.Error.ErrorRequest;
import fr.ul.miage.projet.API.ProjetApiApplication;
import fr.ul.miage.projet.API.assembler.CardAssembler;
import fr.ul.miage.projet.API.entity.Account;
import fr.ul.miage.projet.API.entity.Card;
import fr.ul.miage.projet.API.inputFormat.CardInputFormat;
import fr.ul.miage.projet.API.service.AccountService;
import fr.ul.miage.projet.API.service.CardService;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Optional;

import static org.springframework.hateoas.server.core.DummyInvocationUtils.methodOn;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

@RestController
@RequestMapping(value="/cards", produces = MediaType.APPLICATION_JSON_VALUE)
public class CardController {

    private final CardAssembler cardAss;
    private final CardService cardServ;
    private final AccountService accountServ;

    public CardController(CardAssembler cardAss, CardService cardServ, AccountService accountServ) {
        this.cardAss = cardAss;
        this.cardServ = cardServ;
        this.accountServ = accountServ;
    }

    // Permet de récupérer toutes les transaction
    @GetMapping
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<?> getAllCards() {
        return ResponseEntity.ok(cardAss.toCollectionModel(cardServ.getAllCards()));
    }

    // Permet de récupérer une transaction en fonction de son id
    @GetMapping(value="/{cardId}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<?> getCardById(@PathVariable("cardId") String id) {
        Optional<Card> card = cardServ.getByCardNumber(id);

        if (card.isPresent()) {
            return ResponseEntity.ok(cardAss.toModel(card.get()));
        } else {
            return ErrorRequest.errorRequestMessage("La carte associée à l'id: " + id + " n'existe pas");
        }
    }

    // Permet de créer un account
    @PostMapping("/user/{userId}/account/{iban}/saveCard")
    @PreAuthorize("(hasRole('ROLE_USER') && hasPermission(#userId, 'User', 'MANAGE_USER')) || hasRole('ROLE_ADMIN')")
    public ResponseEntity<?> saveCard(@PathVariable("userId") String userId, @PathVariable("iban") String iban, @RequestBody @Valid CardInputFormat cardVerified) throws Exception {

        Optional<Account> account = accountServ.getAccountByIban(iban);

        // On vérifie que le compte existe
        if (account.isPresent()) {

            // On génère le numéro de la carte qui doit être unique
            Boolean generationDone = false;
            String codeCard = "";
            while (!generationDone) {
                codeCard = ProjetApiApplication.generateCode(16);

                if (!cardServ.getByCardNumber(codeCard).isPresent()) {
                    generationDone = true;
                }
            }

            // On génère le code secret de la carte
            String secretCode = ProjetApiApplication.generateCode(4);

            // On génère le code CCV de la carte
            String ccvCode = ProjetApiApplication.generateCode(3);

            // On génère la date d'expiration de la carte
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            Date date = new Date();
            String dateFormat = formatter.format(date).toString();

            String expiration_date = "";
            Calendar c = Calendar.getInstance();
            c.setTime(date);
            if (cardVerified.getIs_virtual_card()) {
                c.add(Calendar.DATE, 15);
            } else {
                c.add(Calendar.DATE, (365*3));
            }
            expiration_date = formatter.format(c.getTime());

            Card card = cardServ.saveCard(new Card(codeCard, secretCode, ccvCode, expiration_date, false, cardVerified.getLocalisation(), 1500.0, true, cardVerified.getIs_virtual_card(), account.get(), ""));

            URI location = linkTo(methodOn(CardController.class).getCardById(card.getNum_card())).toUri();
            return ResponseEntity.created(location).body(ResponseEntity.ok(cardAss.toModel(card)));
        } else {
            return ErrorRequest.errorRequestMessage("Cet id ne correspond à aucun compte");
        }
    }

    // Permet de bloquer une carte
    @PostMapping(value="/user/{userId}/{cardId}/lockCard")
    @PreAuthorize("(hasRole('ROLE_USER') && hasPermission(#userId, 'User', 'MANAGE_USER')) || hasRole('ROLE_ADMIN')")
    public ResponseEntity<?> lockCard(@PathVariable("userId") String userId, @PathVariable("cardId") String cardNum) {
        Optional<Card> card = cardServ.getByCardNumber(cardNum);

        if (!card.isPresent()) {
            return ErrorRequest.errorRequestMessage("Cet carte accociée au numéro: " + cardNum + " n'existe pas");
        }

        if (!card.get().getAccount().getOwner().getId().equals(userId)) {
            return ErrorRequest.errorRequestMessage("Vous n'avez pas les droits d'accès à cette carte");
        }

        card.get().setBlocked(true);
        Card newCard = cardServ.saveCard(card.get());

        return ResponseEntity.ok(cardAss.toModel(newCard));
    }

    // Permet de débloquer une carte
    @PostMapping(value="/user/{userId}/{cardId}/unlockCard")
    @PreAuthorize("(hasRole('ROLE_USER') && hasPermission(#userId, 'User', 'MANAGE_USER')) || hasRole('ROLE_ADMIN')")
    public ResponseEntity<?> unlockCard(@PathVariable("userId") String userId, @PathVariable("cardId") String cardNum) {
        Optional<Card> card = cardServ.getByCardNumber(cardNum);

        if (!card.isPresent()) {
            return ErrorRequest.errorRequestMessage("Cet carte accociée au numéro: " + cardNum + " n'existe pas");
        }

        if (!card.get().getAccount().getOwner().getId().equals(userId)) {
            return ErrorRequest.errorRequestMessage("Vous n'avez pas les droits d'accès à cette carte");
        }

        card.get().setBlocked(false);
        Card newCard = cardServ.saveCard(card.get());

        return ResponseEntity.ok(cardAss.toModel(newCard));
    }

    // Permet de bloquer les paiement à l'étranger
    @PostMapping(value="/user/{userId}/{cardId}/disablePaiementInForeignCountry")
    @PreAuthorize("(hasRole('ROLE_USER') && hasPermission(#userId, 'User', 'MANAGE_USER')) || hasRole('ROLE_ADMIN')")
    public ResponseEntity<?> disablePaiementInForeignCountry(@PathVariable("userId") String userId, @PathVariable("cardId") String cardNum) {
        Optional<Card> card = cardServ.getByCardNumber(cardNum);

        if (!card.isPresent()) {
            return ErrorRequest.errorRequestMessage("Cet carte accociée au numéro: " + cardNum + " n'existe pas");
        }

        if (!card.get().getAccount().getOwner().getId().equals(userId)) {
            return ErrorRequest.errorRequestMessage("Vous n'avez pas les droits d'accès à cette carte");
        }

        card.get().setLocalisation(false);
        Card newCard = cardServ.saveCard(card.get());

        return ResponseEntity.ok(cardAss.toModel(newCard));
    }

    // Permet de débloquer les paiements à l'étranger
    @PostMapping(value="/user/{userId}/{cardId}/enablePaiementInForeignCountry")
    @PreAuthorize("(hasRole('ROLE_USER') && hasPermission(#userId, 'User', 'MANAGE_USER')) || hasRole('ROLE_ADMIN')")
    public ResponseEntity<?> enablePaiementInForeignCountry(@PathVariable("userId") String userId, @PathVariable("cardId") String cardNum) {
        Optional<Card> card = cardServ.getByCardNumber(cardNum);

        if (!card.isPresent()) {
            return ErrorRequest.errorRequestMessage("Cet carte accociée au numéro: " + cardNum + " n'existe pas");
        }

        if (!card.get().getAccount().getOwner().getId().equals(userId)) {
            return ErrorRequest.errorRequestMessage("Vous n'avez pas les droits d'accès à cette carte");
        }

        card.get().setLocalisation(true);
        Card newCard = cardServ.saveCard(card.get());

        return ResponseEntity.ok(cardAss.toModel(newCard));
    }

    // Permet de désactiver les paiements sans contact
    @PostMapping(value="/user/{userId}/{cardId}/disableWithoutContact")
    @PreAuthorize("(hasRole('ROLE_USER') && hasPermission(#userId, 'User', 'MANAGE_USER')) || hasRole('ROLE_ADMIN')")
    public ResponseEntity<?> disableWithoutContact(@PathVariable("userId") String userId, @PathVariable("cardId") String cardNum) {
        Optional<Card> card = cardServ.getByCardNumber(cardNum);

        if (!card.isPresent()) {
            return ErrorRequest.errorRequestMessage("Cet carte accociée au numéro: " + cardNum + " n'existe pas");
        }

        if (!card.get().getAccount().getOwner().getId().equals(userId)) {
            return ErrorRequest.errorRequestMessage("Vous n'avez pas les droits d'accès à cette carte");
        }

        card.get().setWithout_contact(false);
        Card newCard = cardServ.saveCard(card.get());

        return ResponseEntity.ok(cardAss.toModel(newCard));
    }

    // Permet d'activer les paiements sans contact
    @PostMapping(value="/user/{userId}/{cardId}/enableWithoutContact")
    @PreAuthorize("(hasRole('ROLE_USER') && hasPermission(#userId, 'User', 'MANAGE_USER')) || hasRole('ROLE_ADMIN')")
    public ResponseEntity<?> enableWithoutContact(@PathVariable("userId") String userId, @PathVariable("cardId") String cardNum) {
        Optional<Card> card = cardServ.getByCardNumber(cardNum);

        if (!card.isPresent()) {
            return ErrorRequest.errorRequestMessage("Cet carte accociée au numéro: " + cardNum + " n'existe pas");
        }

        if (!card.get().getAccount().getOwner().getId().equals(userId)) {
            return ErrorRequest.errorRequestMessage("Vous n'avez pas les droits d'accès à cette carte");
        }

        card.get().setWithout_contact(true);
        Card newCard = cardServ.saveCard(card.get());

        return ResponseEntity.ok(cardAss.toModel(newCard));
    }

}
