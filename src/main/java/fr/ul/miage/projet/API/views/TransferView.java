package fr.ul.miage.projet.API.views;

import fr.ul.miage.projet.API.entity.Transfer;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.hateoas.RepresentationModel;

@Data
@Getter
@Setter
public class TransferView extends RepresentationModel<TransactionView> {

    private String id;
    private String date;
    private String libelle;
    private Double amount;
    private Double rate_apply;
    private String debtor_account_iban;
    private String target_account_iban;

    public TransferView(Transfer transfer) {
        this.id = transfer.getId();
        this.date = transfer.getDate();
        this.libelle = transfer.getLibelle();
        this.amount = transfer.getAmount();
        this.rate_apply = transfer.getRate_apply();
        this.debtor_account_iban = transfer.getDebtor_account().getIban();
        this.target_account_iban = transfer.getTarget_account().getIban();
    }
}
