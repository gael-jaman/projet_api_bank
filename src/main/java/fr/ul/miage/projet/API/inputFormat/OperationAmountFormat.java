package fr.ul.miage.projet.API.inputFormat;

import lombok.*;

@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class OperationAmountFormat {

    private Double amount;

    private String currency;
}
