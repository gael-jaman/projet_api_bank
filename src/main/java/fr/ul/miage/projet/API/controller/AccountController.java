package fr.ul.miage.projet.API.controller;

import fr.ul.miage.projet.API.Error.ErrorRequest;
import fr.ul.miage.projet.API.ProjetApiApplication;
import fr.ul.miage.projet.API.assembler.AccountAssembler;
import fr.ul.miage.projet.API.entity.Account;
import fr.ul.miage.projet.API.entity.User;
import fr.ul.miage.projet.API.inputFormat.AccountInputFormat;
import fr.ul.miage.projet.API.inputFormat.CountryDeviseAndIBANkeyFormat;
import fr.ul.miage.projet.API.service.AccountService;
import fr.ul.miage.projet.API.service.UserService;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import static org.springframework.hateoas.server.core.DummyInvocationUtils.methodOn;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.util.*;

@RestController
@RequestMapping(value="/accounts", produces = MediaType.APPLICATION_JSON_VALUE)
public class AccountController {

    private final AccountAssembler accountAss;
    private final AccountService accountServ;
    private final UserService userServ;

    public static HashMap<String, CountryDeviseAndIBANkeyFormat> country_devise_ibanCode = new HashMap<String, CountryDeviseAndIBANkeyFormat>();
    public static HashMap<String, HashMap<String, Double>> exchange_rates = new HashMap<String, HashMap<String, Double>>();

    public AccountController(AccountAssembler accountAss, UserService userserv, AccountService accountServ) {
        this.accountAss = accountAss;
        this.accountServ = accountServ;
        this.userServ = userserv;

        country_devise_ibanCode.put("France", new CountryDeviseAndIBANkeyFormat("EUR", "FR"));
        country_devise_ibanCode.put("France métropolitaine", new CountryDeviseAndIBANkeyFormat("EUR", "FR"));
        country_devise_ibanCode.put("Allemagne", new CountryDeviseAndIBANkeyFormat("EUR", "DE"));
        country_devise_ibanCode.put("Autriche", new CountryDeviseAndIBANkeyFormat("EUR", "AT"));
        country_devise_ibanCode.put("Belgique", new CountryDeviseAndIBANkeyFormat("EUR", "BE"));
        country_devise_ibanCode.put("Chypre", new CountryDeviseAndIBANkeyFormat("EUR", "CY"));
        country_devise_ibanCode.put("Espagne", new CountryDeviseAndIBANkeyFormat("EUR", "ES"));
        country_devise_ibanCode.put("Estonie", new CountryDeviseAndIBANkeyFormat("EUR", "EE"));
        country_devise_ibanCode.put("Finlande", new CountryDeviseAndIBANkeyFormat("EUR", "FI"));
        country_devise_ibanCode.put("Grèce", new CountryDeviseAndIBANkeyFormat("EUR", "GR"));
        country_devise_ibanCode.put("Guadeloupe", new CountryDeviseAndIBANkeyFormat("EUR", "GI"));
        country_devise_ibanCode.put("Guyane française", new CountryDeviseAndIBANkeyFormat("EUR", "GI"));
        country_devise_ibanCode.put("Irlande", new CountryDeviseAndIBANkeyFormat("EUR", "IE"));
        country_devise_ibanCode.put("Italie", new CountryDeviseAndIBANkeyFormat("EUR", "IT"));
        country_devise_ibanCode.put("Lettonie", new CountryDeviseAndIBANkeyFormat("EUR", "LV"));
        country_devise_ibanCode.put("Luxembourg", new CountryDeviseAndIBANkeyFormat("EUR", "LU"));
        country_devise_ibanCode.put("Malte", new CountryDeviseAndIBANkeyFormat("EUR", "MT"));
        country_devise_ibanCode.put("Diverses îles des Etats-Unis", new CountryDeviseAndIBANkeyFormat("USD", "US"));
        country_devise_ibanCode.put("Equateur", new CountryDeviseAndIBANkeyFormat("USD", "US"));
        country_devise_ibanCode.put("Etats-Unis", new CountryDeviseAndIBANkeyFormat("USD", "US"));
        country_devise_ibanCode.put("Guam", new CountryDeviseAndIBANkeyFormat("USD", "US"));
        country_devise_ibanCode.put("Micronésie", new CountryDeviseAndIBANkeyFormat("USD", "US"));
        country_devise_ibanCode.put("Palaos", new CountryDeviseAndIBANkeyFormat("USD", "US"));
        country_devise_ibanCode.put("Japon", new CountryDeviseAndIBANkeyFormat("YEN", "YE"));
        country_devise_ibanCode.put("Georgie du Sud et Îles Sandwich du Sud", new CountryDeviseAndIBANkeyFormat("GBP", "GB"));
        country_devise_ibanCode.put("Grande-Bretagne", new CountryDeviseAndIBANkeyFormat("GBP", "GB"));
        country_devise_ibanCode.put("Territoires britanniques de l'océan indien", new CountryDeviseAndIBANkeyFormat("GBP", "GB"));
        country_devise_ibanCode.put("Canada", new CountryDeviseAndIBANkeyFormat("CAD", "CA"));

        // On initialise les taux de change des différents pays
        exchange_rates = new HashMap<String, HashMap<String, Double>>();
        // Taux de change de l'euro vers une autre monnaie
        HashMap<String, Double> eur_convert = new HashMap<String, Double>();
        eur_convert.put("USD", 1.14);eur_convert.put("YEN", 131.31);eur_convert.put("GBP", 0.84);eur_convert.put("CAD", 1.44);
        exchange_rates.put("EUR", eur_convert);

        // Taux de change du dollar américain vers une autre monnaie
        HashMap<String, Double> usd_convert = new HashMap<String, Double>();
        usd_convert.put("EUR", 0.88);usd_convert.put("YEN", 115.59);usd_convert.put("GBP", 0.74);usd_convert.put("CAD", 1.26);
        exchange_rates.put("USD", usd_convert);

        // Taux de change du Yen vers une autre monnaie
        HashMap<String, Double> yen_convert = new HashMap<String, Double>();
        yen_convert.put("EUR", 0.0076);yen_convert.put("USD", 0.0087);yen_convert.put("GBP", 0.0064);yen_convert.put("CAD", 0.011);
        exchange_rates.put("YEN", yen_convert);

        // Taux de change du gbp vers une autre monnaie
        HashMap<String, Double> gbp_convert = new HashMap<String, Double>();
        gbp_convert.put("EUR", 0.0076);gbp_convert.put("USD", 0.0087);gbp_convert.put("YEN", 0.0064);gbp_convert.put("CAD", 0.011);
        exchange_rates.put("GBP", gbp_convert);

        // Taux de change du Yen vers une autre monnaie
        HashMap<String, Double> cad_convert = new HashMap<String, Double>();
        cad_convert.put("EUR", 0.70);cad_convert.put("USD", 0.79);cad_convert.put("YEN", 91.43);cad_convert.put("GBP", 0.58);
        exchange_rates.put("CAD", cad_convert);
    }

    // Permet de récupérer tous les comptes
    @GetMapping
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<?> getAllAccounts() {
        return ResponseEntity.ok(accountAss.toCollectionModel(accountServ.getAllAccounts()));
    }

    // Permet de créer un account
    @PostMapping("/user/{userId}/saveAccount")
    @PreAuthorize("(hasRole('ROLE_USER') && hasPermission(#userId, 'User', 'MANAGE_USER')) || hasRole('ROLE_ADMIN')")
    public ResponseEntity<?> saveAccount(@PathVariable("userId") String userId, @RequestBody @Valid AccountInputFormat accountVerified) throws Exception {

        Optional<User> user = userServ.getUserById(userId);

        // On vérifie que l'utilisateur existe bien
        if (user.isPresent()) {
            // On vérifie que le pays fais bien partie de la liste
            if (this.country_devise_ibanCode.containsKey(accountVerified.getCountry())) {

                // Génération de l'iban, nous avons besoin de 25 nombres
                String codeIban = "";
                Boolean generationDone = false;
                while (!generationDone) {
                    codeIban = this.country_devise_ibanCode.get(accountVerified.getCountry()).getIbanCode() + ProjetApiApplication.generateCode(25);

                    if (!accountServ.getAccountByIban(codeIban).isPresent()) {
                        generationDone = true;
                    }
                }

                // On crée le compte à partir des données générés
                Account account = accountServ.saveAccount(new Account(codeIban, 0.0, accountVerified.getCountry(), user.get()));

                URI location = linkTo(methodOn(AccountController.class).getAccountByIBAN(account.getIban())).toUri();
                return ResponseEntity.created(location).body(ResponseEntity.ok(accountAss.toModel(account)));
            } else {
                return ErrorRequest.errorRequestMessage("Ce pays ne fais pas parti de la liste");
            }
        } else {
            return ErrorRequest.errorRequestMessage("Cet id ne correspond à aucun utilisateur");
        }
    }

    // Permet de récupérer un compte en fonction de son iban
    @GetMapping(value="/{iban}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<?> getAccountByIBAN(@PathVariable("iban") String iban) { // Cette méthode permet de voir en détail les informations du compte
        Optional<Account> account = accountServ.getAccountByIban(iban);

        if (account.isPresent()) {
            return ResponseEntity.ok(accountAss.toModel(account.get()));
        } else {
            return ErrorRequest.errorRequestMessage("Le compte associé à l'iban: " + iban + " n'existe pas");
        }
    }

    // Permet de récupérer tous les comptes d'un utilisateur
    @GetMapping(value="/user/{userId}")
    @PreAuthorize("(hasRole('ROLE_USER') && hasPermission(#userId, 'User', 'MANAGE_USER')) || hasRole('ROLE_ADMIN')")
    public ResponseEntity<?> getAccountsByUserId(@PathVariable("userId") String userId) { // Cette méthode permet de voir en détail les informations du compte
        List<Account> listAccount = accountServ.getAccountsByUserId(userId);

        System.out.println(listAccount);

        if (!listAccount.isEmpty()) {
            return ResponseEntity.ok(accountAss.toCollectionModel(listAccount));
        } else {
            return ErrorRequest.errorRequestMessage("Cet utilisateur ne possède aucun compte");
        }
    }


}
