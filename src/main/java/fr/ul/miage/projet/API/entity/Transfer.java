package fr.ul.miage.projet.API.entity;

import fr.ul.miage.projet.API.controller.AccountController;
import fr.ul.miage.projet.API.inputFormat.OperationAmountFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@NoArgsConstructor
public class Transfer implements Serializable { // Cette entité représente les opérations effectués à l'aide d'une carte bancaire

    private static final long serialVersionUID = 6213487251L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private String id;
    private String date;
    private String libelle;
    private Double amount; // Le montant est enregistré dans la monnaie du pays de provenance
    private Double rate_apply;
    @ManyToOne
    @JoinColumn(name = "debtor_account_iban")
    @ApiModelProperty(hidden = true)
    private Account debtor_account;
    @ManyToOne
    @JoinColumn(name = "target_account_iban")
    private Account target_account;

    public Transfer(String id, String date, String libelle, Double amount, Double rate_apply, Account debtor_account, Account target_account) {
        this.id = id;
        this.date = date;
        this.libelle = libelle;
        this.amount = amount;
        this.rate_apply = rate_apply;
        this.debtor_account = debtor_account;
        this.target_account = target_account;
    }

    // Permet de récupérer le montant de la transaction avec la devise associée pour la personne ayant envoyé l'argent
    public OperationAmountFormat getAmountFromSourceCountry() {
        // On récupère le type de monnaie
        String currency = AccountController.country_devise_ibanCode.get(this.debtor_account.getCountry()).getDevise();
        return new OperationAmountFormat(this.amount, currency);
    }

    // Permet de récupérer le montant de la transaction avec la devise associée pour la personne ayant reçu l'argent
    public OperationAmountFormat getAmountFromDestinationCountry() {
        // On récupère le type de monnaie
        String currency = AccountController.country_devise_ibanCode.get(this.target_account.getCountry()).getDevise();
        Double convertedAmount = this.amount*this.rate_apply;
        return new OperationAmountFormat(convertedAmount, currency);
    }
}