package fr.ul.miage.projet.API.serviceImplementation;

import fr.ul.miage.projet.API.entity.Account;
import fr.ul.miage.projet.API.repository.AccountRepository;
import fr.ul.miage.projet.API.service.AccountService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Transactional
@Slf4j
public class AccountServiceImplement implements AccountService {

    private final AccountRepository accountRepo;

    // Permet de sauvegarder un account
    @Override
    public Account saveAccount(Account account) {
        log.info("Sauvegarde d'un nouveau compte pour l'utilisateur: " + account.getOwner().getEmail());
        return accountRepo.save(account);
    }

    // Permet de récupérer un compte en fonction de son iban
    @Override
    public Optional<Account> getAccountByIban(String iban) {
        log.info("On récupère le compte en fonction de l'iban: " + iban);
        return accountRepo.findById(iban);
    }

    // Permet de récupérer un compte en fonction de son iban
    @Override
    public List<Account> getAccountsByUserId(String userId) {
        log.info("On récupère tous les comptes de l'utilisateur : " + userId);
        List<Account> listAccounts = accountRepo.findAll();

        ArrayList<Account> listToShow = new ArrayList<Account>();
        for (Account account : listAccounts) {
            System.out.println("id account: " + account.getOwner().getId());
            if (account.getOwner().getId().equals(userId)) {
                listToShow.add(account);
            }
        }

        return listToShow;
    }

    // Permet de récupérer tous les utilisateurs
    @Override
    public List<Account> getAllAccounts() {
        log.info("On récupère tous les comptes");
        return accountRepo.findAll();
    }

    // Permet de savoir si le compte possède suffisament d'argent pour effectuer l'opération
    @Override
    public boolean hasEnoughAmount(Account account, Double amount) {
        log.info("On vérifie si le compte possède suffisament d'argent");

        Boolean hasEnoughAmnt = false;
        if (account.getBalance() >= amount) {
            hasEnoughAmnt = true;
            log.info("-----Le compte possède suffisament d'argent");
        } else {
            log.info("-----Le compte ne possède pas suffisament d'argent");
        }

        return hasEnoughAmnt;
    }

    // Permet de retirer de l'argent à un compte
    @Override
    public Account decreaseBalance(Account account, Double amount) {
        log.info("On retire de l'argent au compte: " + account.getIban() + ", balance actuel: " + account.getBalance());

        if (hasEnoughAmount(account, amount)) {
            Double newBalance = account.getBalance() - amount;
            log.info("La nouvel balance du compte est: " + newBalance);
            account.setBalance(newBalance);
            return saveAccount(account);
        }

        log.info("Le compte ne possède pas assez d'argent");
        return account;
    }

    // Permet d'ajouter de l'argent à un compte
    @Override
    public Account increaseBalance(Account account, Double amount) {
        log.info("On ajoute de l'argent au compte: " + account.getIban() + ", balance actuel: " + account.getBalance());

        Double newBalance = account.getBalance() + amount;
        log.info("La nouvel balance du compte est: " + newBalance);
        account.setBalance(newBalance);
        return saveAccount(account);
    }
}
