package fr.ul.miage.projet.API.entity;

import fr.ul.miage.projet.API.inputFormat.CountryDeviseAndIBANkeyFormat;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashMap;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Account implements Serializable {

    private static final long serialVersionUID = 7290152783L;

    @Id
    @Column(name = "iban", nullable = false)
    private String iban; // L'iban est un numéro unique et peut par conséquent être utilisé comme id pour le compte
    private Double balance; // Argent disponible sur le compte
    private String country; // Pays dans lequel le compte est localisé
    //private String secret;
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User owner;

    public Account(String iban, Double balance, String country, User owner) {
        this.iban = iban;
        this.balance = balance;
        this.country = country;
        this.owner = owner;
    }
}