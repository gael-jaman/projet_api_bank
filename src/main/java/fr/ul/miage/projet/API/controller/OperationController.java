package fr.ul.miage.projet.API.controller;

import fr.ul.miage.projet.API.Error.ErrorRequest;
import fr.ul.miage.projet.API.assembler.*;
import fr.ul.miage.projet.API.entity.*;
import fr.ul.miage.projet.API.inputFormat.DepositAndWithdrawInputFormat;
import fr.ul.miage.projet.API.inputFormat.TransactionInternetInputFormat;
import fr.ul.miage.projet.API.inputFormat.TransactionTPEInputFormat;
import fr.ul.miage.projet.API.inputFormat.TransferInputFormat;
import fr.ul.miage.projet.API.service.*;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.springframework.hateoas.server.core.DummyInvocationUtils.methodOn;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

@RestController
@RequestMapping(value="/operations", produces = MediaType.APPLICATION_JSON_VALUE)
public class OperationController {

    private final TransferAssembler transferAss;
    private final TransferService transferServ;
    private final TransactionAssembler transactionAss;
    private final TransactionService transactionServ;
    private final AccountService accountServ;
    private final AccountAssembler accountAss;
    private final CardService cardServ;
    private final TransferViewAssembler transferViewAssembler;
    private final TransactionViewAssembler transactionViewAssembler;
    private final UserService userServ;

    public OperationController(TransferAssembler transferAss, TransferService transferServ, TransactionAssembler transactionAss, TransactionService transactionServ, AccountService accountServ, AccountAssembler accountAss, CardService cardServ, TransferViewAssembler transferViewAssembler, TransactionViewAssembler transactionViewAssembler, UserService userServ) {
        this.transferAss = transferAss;
        this.transferServ = transferServ;
        this.transactionAss = transactionAss;
        this.transactionServ = transactionServ;
        this.accountServ = accountServ;
        this.accountAss = accountAss;
        this.cardServ = cardServ;
        this.transferViewAssembler = transferViewAssembler;
        this.transactionViewAssembler = transactionViewAssembler;
        this.userServ = userServ;
    }

    @GetMapping()
    public ResponseEntity<?> root() {
        return ErrorRequest.errorRequestMessage("Cette route ne permet de pas récupérer des informations. Merci d'indiquer une autre root");
    }

    // GESTION DES VIREMENTS
    // Permet de récupérer toutes les transaction
    @GetMapping("/transfers")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<?> getAllTransfers() {
        return ResponseEntity.ok(transferAss.toCollectionModel(transferServ.getAllTransfers()));
    }

    // Permet de récupérer une transaction en fonction de son id
    @GetMapping(value="/transfer/{transferId}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<?> getTransferById(@PathVariable("transferId") String id) {
        Optional<Transfer> transfer = transferServ.getById(id);

        if (transfer.isPresent()) {
            return ResponseEntity.ok(transferViewAssembler.toModel(transfer.get()));
        } else {
            return ErrorRequest.errorRequestMessage("Le virement associée à l'id: " + id + " n'existe pas");
        }
    }

    // Permet de récupérer les virements d'un utilisateur
    @GetMapping(value="/transfer/user/{userId}")
    @PreAuthorize("(hasRole('ROLE_USER') && hasPermission(#userId, 'User', 'MANAGE_USER')) || hasRole('ROLE_ADMIN')")
    public ResponseEntity<?> getTransfersByUserId(@PathVariable("userId") String userId) {
        List<Transfer> listTransfers = transferServ.getAllTransfers();

        if (!listTransfers.isEmpty()) {
            ArrayList<Transfer> listToShow = new ArrayList<Transfer>();

            for (Transfer transfer : listTransfers) {
                if (transfer.getTarget_account().getOwner().getId().equals(userId) || transfer.getDebtor_account().getOwner().getId().equals(userId)) {
                    listToShow.add(transfer);
                }
            }

            if (!listToShow.isEmpty()) {
                return ResponseEntity.ok(transferViewAssembler.toCollectionModel(listToShow));
            } else {
                return ErrorRequest.errorRequestMessage("Cet utilisateur ne possède aucun compte");
            }
        } else {
            return ErrorRequest.errorRequestMessage("Aucun comptes disponibles");
        }
    }

    // Permet de sauvegarder un virement
    @PostMapping(value="/user/{userId}/transfer/save")
    @PreAuthorize("(hasRole('ROLE_USER') && hasPermission(#userId, 'User', 'MANAGE_USER')) || hasRole('ROLE_ADMIN')")
    public ResponseEntity<?> saveTransfer(@PathVariable("userId") String userId, @RequestBody @Valid TransferInputFormat transferVerified) {

        // On vérifie que me montant de la transaction est bien supérieur à 0
        if (transferVerified.getAmount() > 0) {
            // On vérifie si le compte débiteur existe
            Optional<Account> accountDebtor = accountServ.getAccountByIban(transferVerified.getDebtor_iban());

            if (accountDebtor.isPresent()) {

                // On vérifie que le compte débiteur appartient bien à la personne qui effectue le virement
                if (!accountDebtor.get().getOwner().getId().equals(userId)) {
                    return ErrorRequest.errorRequestMessage("Le compte débiteur ne correspond pas à l'id insérer en paramètre");
                }

                // On vérifie si le compte débiteur possède assez d'argent
                if (accountDebtor.get().getBalance() >= transferVerified.getAmount()) {

                    // On vérifie si le compte créditeur existe
                    Optional<Account> accountTarget = accountServ.getAccountByIban(transferVerified.getTarget_iban());
                    if (accountTarget.isPresent()) {

                        // On récupère la date de virement
                        SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");
                        Date date = new Date();
                        String currentDate = formatter.format(date).toString();

                        // On récupère le taux à appliquer en fonction des pays d'origine des comptes
                        String currency_debtor = AccountController.country_devise_ibanCode.get(accountDebtor.get().getCountry()).getDevise();
                        String currency_target = AccountController.country_devise_ibanCode.get(accountTarget.get().getCountry()).getDevise();

                        Double rateToApply = 1.0;
                        if (!currency_debtor.equals(currency_target)) { // Si le virement se fais entre deux compte qui ont des monnaies différentes alors on récupère le taux de change
                            rateToApply = AccountController.exchange_rates.get(currency_debtor).get(currency_target);
                        }


                        Transfer newTransfer = transferServ.saveTransfer(new Transfer(UUID.randomUUID().toString(), currentDate, transferVerified.getLibelle(), transferVerified.getAmount(), rateToApply, accountDebtor.get(), accountTarget.get()));

                        // Si tout s'est exécuté comme il faut alors on peut effectuer le virement
                        // On débite le compte
                        accountDebtor.get().setBalance(accountDebtor.get().getBalance()-transferVerified.getAmount());
                        accountServ.saveAccount(accountDebtor.get());
                        // Et enfin on crédite l'autre compte
                        accountTarget.get().setBalance(accountTarget.get().getBalance()+(transferVerified.getAmount()*rateToApply));
                        accountServ.saveAccount(accountTarget.get());

                        URI location = linkTo(methodOn(OperationController.class).getTransferById(newTransfer.getId())).toUri();
                        return ResponseEntity.created(location).body(ResponseEntity.ok(transferViewAssembler.toModel(newTransfer)));
                    } else {
                        return ErrorRequest.errorRequestMessage("Le virement a du être annulé car le compte créditeur possédant l'iban: " + transferVerified.getTarget_iban() + " n'existe pas");
                    }
                } else {
                    return ErrorRequest.errorRequestMessage("Le virement a du être annulé car le compte débiteur ne possède pas assez d'argent");
                }
            } else {
                return ErrorRequest.errorRequestMessage("Le virement a du être annulé car le compte débiteur possédant l'iban: " + transferVerified.getDebtor_iban() + " n'existe pas");
            }
        } else {
            return ErrorRequest.errorRequestMessage("Le virement a du être annulé car le montant de la transation doit être supérieur à 0");
        }
    }

    // Permet d'ajouter de l'argent à un compte
    @PostMapping(value="/user/{userId}/account/{iban}/deposit")
    @PreAuthorize("(hasRole('ROLE_USER') && hasPermission(#userId, 'User', 'MANAGE_USER')) || hasRole('ROLE_ADMIN')")
    public ResponseEntity<?> depositMoneyToAccount(@PathVariable("userId") String userId, @PathVariable("iban") String iban, @RequestBody @Valid DepositAndWithdrawInputFormat amountVerified) {
        if (amountVerified.getAmount() > 0) { // On vérifie si le montant est supérieur à 0

            // On vérifie si le compte existe
            Optional<Account> account = accountServ.getAccountByIban(iban);
            if (account.isPresent()) {

                // On vérifie que le compte débiteur appartient bien à la personne qui effectue le virement
                if (!account.get().getOwner().getId().equals(userId)) {
                    return ErrorRequest.errorRequestMessage("L'id du compte ne correspond pas avec l'id insérer en paramètre");
                }

                Account accountSave = accountServ.increaseBalance(account.get(), amountVerified.getAmount());

                return ResponseEntity.ok(accountAss.toModel(accountSave));
            } else {
                return ErrorRequest.errorRequestMessage("Le compte associé à l'iban: " + iban + " n'existe pas");
            }
        } else {
            return ErrorRequest.errorRequestMessage("Un dépot d'argent nécessite une somme supérieur à 0");
        }
    }

    // Permet de retirer de l'argent à un compte
    @PostMapping(value="/user/{userId}/account/{iban}/withdraw")
    @PreAuthorize("(hasRole('ROLE_USER') && hasPermission(#userId, 'User', 'MANAGE_USER')) || hasRole('ROLE_ADMIN')")
    public ResponseEntity<?> withdrawMoneyToAccount(@PathVariable("userId") String userId, @PathVariable("iban") String iban, @RequestBody @Valid DepositAndWithdrawInputFormat amountVerified) {
        if (amountVerified.getAmount() > 0) { // On vérifie si le montant est supérieur à 0

            // On vérifie si le compte existe
            Optional<Account> account = accountServ.getAccountByIban(iban);
            if (account.isPresent()) {

                // On vérifie que le compte débiteur appartient bien à la personne qui effectue le virement
                if (!account.get().getOwner().getId().equals(userId)) {
                    return ErrorRequest.errorRequestMessage("L'id du compte ne correspond pas avec l'id insérer en paramètre");
                }

                // On vérifie si le compte possède suffisament d'argent pour effectuer le retrait
                if (account.get().getBalance() >= amountVerified.getAmount()) {

                    // On effectue le retrait d'argent
                    Account accountSave = accountServ.decreaseBalance(account.get(), amountVerified.getAmount());

                    return ResponseEntity.ok(accountAss.toModel(accountSave));
                } else {
                    return ErrorRequest.errorRequestMessage("Il n'y a pas assez d'argent sur le compte pour effectuer ce retrait");
                }
            } else {
                return ErrorRequest.errorRequestMessage("Le compte associé à l'iban: " + iban + " n'existe pas");
            }
        } else {
            return ErrorRequest.errorRequestMessage("Pour retirer de l'argent il faut insérer une somme supérieur à 0");
        }
    }


    //GESTION DES PAIEMENTS PAR CARTE BANCAIRE

    // Permet de récupérer toutes les transactions
    @GetMapping("/transactions")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<?> getAllTransactions() {
        return ResponseEntity.ok(transactionAss.toCollectionModel(transactionServ.getAllTransactions()));
    }

    // Permet de récupérer une transaction spécifique
    @GetMapping(value="/transaction/{transactionId}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<?> getTransactionById(@PathVariable("transactionId") String id) {
        Optional<Transaction> transaction = transactionServ.getById(id);

        if (transaction.isPresent()) {
            return ResponseEntity.ok(transactionViewAssembler.toModel(transaction.get()));
        } else {
            return ErrorRequest.errorRequestMessage("La transaction associée à l'id: " + id + " n'existe pas");
        }
    }

    // Permet de récupérer les transactions d'un utilisateur
    @GetMapping(value="/transaction/user/{userId}")
    @PreAuthorize("(hasRole('ROLE_USER') && hasPermission(#userId, 'User', 'MANAGE_USER')) || hasRole('ROLE_ADMIN')")
    public ResponseEntity<?> getTransactionsByUserId(@PathVariable("userId") String userId) {
        List<Transaction> listTransactions = transactionServ.getAllTransactions();

        if (!listTransactions.isEmpty()) {
            ArrayList<Transaction> listToShow = new ArrayList<Transaction>();

            for (Transaction transaction : listTransactions) {
                if (transaction.getCard().getAccount().getOwner().getId().equals(userId) || transaction.getReceiverAccount().getOwner().getId().equals(userId)) {
                    listToShow.add(transaction);
                }
            }

            if (!listToShow.isEmpty()) {
                return ResponseEntity.ok(transactionViewAssembler.toCollectionModel(listToShow));
            } else {
                return ErrorRequest.errorRequestMessage("Cet utilisateur ne possède aucun compte");
            }
        } else {
            return ErrorRequest.errorRequestMessage("Aucun comptes disponibles");
        }
    }

    // Permet de sauvegarder un paiement via un TPE
    @PostMapping(value="/user/{userId}/transaction/saveTPEtransaction")
    @PreAuthorize("(hasRole('ROLE_USER') && hasPermission(#userId, 'User', 'MANAGE_USER')) || hasRole('ROLE_ADMIN')")
    public ResponseEntity<?> saveTransactionFromTPE(@PathVariable("userId") String userId, @RequestBody @Valid TransactionTPEInputFormat transactionVerified) throws ParseException {

        // On vérifie que me montant est bien supérieur à 0
        if (transactionVerified.getAmount() > 0) {
            // On vérifie si le compte qui va recevoir l'argent existe
            Optional<Account> account_target = accountServ.getAccountByIban(transactionVerified.getReceiver_account_iban());

            if (account_target.isPresent()) {

                Optional<Card> card = cardServ.getByCardNumber(transactionVerified.getNum_card());
                // On vérifie si la carte existe
                if (card.isPresent()) {

                    // On vérifie que le compte débiteur appartient bien à la personne qui effectue le virement
                    if (!card.get().getAccount().getOwner().getId().equals(userId)) {
                        return ErrorRequest.errorRequestMessage("L'id du compte débiteur ne correspond pas avec l'id insérer en paramètre");
                    }

                    // On vérifie si le paiement sans contact est possible
                    if (!card.get().getWithout_contact() && transactionVerified.getIs_without_contact_paiement()) {
                        return ErrorRequest.errorRequestMessage("Les paiements sans contact sont désactivés");
                    }

                    // On vérifie si le code secret de la carte correspond
                    if (card.get().getSecret_code().equals(transactionVerified.getSecret_code())) {

                        // On vérifie que la carte est toujours valide
                        if (cardServ.isAlreadyValidCard(card.get())) {

                            // On vérifie que le montant de paiement ne dépasse pas le plafond de la carte et que l'utilisateur possède assez d'argent
                            if (card.get().getCeiling() >= transactionVerified.getAmount() && card.get().getAccount().getBalance() >= transactionVerified.getAmount()) {

                                // On vérifie si la carte n'as pas été supprimée
                                if (card.get().getDeleted_date().equals("")) {
                                    // On vérifie que la carte n'est pas bloquée
                                    if (!card.get().getBlocked()) {
                                        // On récupère la date current
                                        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                                        Date date = new Date();
                                        String currentDate = formatter.format(date).toString();

                                        // On récupère le taux à appliquer en fonction des pays d'origine des comptes
                                        String currency_debtor = AccountController.country_devise_ibanCode.get(card.get().getAccount().getCountry()).getDevise();
                                        String currency_target = AccountController.country_devise_ibanCode.get(account_target.get().getCountry()).getDevise();

                                        // On vérifie si les paiements à l'étranger sont possibles
                                        if (!card.get().getLocalisation() && !currency_debtor.equals(currency_target)) {
                                            return ErrorRequest.errorRequestMessage("Les paiements à l'étranger sont désactivés");
                                        }

                                        Double rateToApply = 1.0;
                                        if (!currency_debtor.equals(currency_target)) { // Si le paiement se fais entre deux compte qui ont des monnaies différentes alors on récupère le taux de change
                                            rateToApply = AccountController.exchange_rates.get(currency_debtor).get(currency_target);
                                        }

                                        Transaction newTransaction = transactionServ.saveTransaction(new Transaction(UUID.randomUUID().toString(), currentDate, transactionVerified.getLibelle(), transactionVerified.getAmount(), rateToApply, card.get(), account_target.get()));

                                        // Si tout s'est exécuté comme il faut alors on peut effectuer le paiement
                                        // On débite le compte
                                        card.get().getAccount().setBalance(card.get().getAccount().getBalance()-transactionVerified.getAmount());
                                        cardServ.saveCard(card.get());
                                        // Et enfin on crédite l'autre compte
                                        account_target.get().setBalance(account_target.get().getBalance()+(transactionVerified.getAmount()*rateToApply));
                                        accountServ.saveAccount(account_target.get());

                                        URI location = linkTo(methodOn(OperationController.class).getTransactionById(newTransaction.getId())).toUri();
                                        return ResponseEntity.created(location).body(ResponseEntity.ok(transactionViewAssembler.toModel(newTransaction)));
                                    } else {
                                        return ErrorRequest.errorRequestMessage("La carte est bloquée");
                                    }
                                } else {
                                    return ErrorRequest.errorRequestMessage("La carte est supprimée et par conséquent inutilisable");
                                }
                            } else {
                                return ErrorRequest.errorRequestMessage("Le montant dépasse le plafond de la carte");
                            }
                        } else {
                            // On supprime la carte car elle n'est plus valide
                            cardServ.deleteCard(card.get());

                            return ErrorRequest.errorRequestMessage("La carte renseignée n'est plus valide");
                        }
                    } else {
                        return ErrorRequest.errorRequestMessage("La carte renseignée n'existe pas das le system, merci de vérifier vos informations");
                    }
                } else {
                    return ErrorRequest.errorRequestMessage("La carte renseignée n'existe pas das le system, merci de vérifier vos informations");
                }
            } else {
                return ErrorRequest.errorRequestMessage("Le paiement a du être annulé car le compte recevant l'argent identidié par l'iban : " + transactionVerified.getReceiver_account_iban() + " n'existe pas");
            }
        } else {
            return ErrorRequest.errorRequestMessage("Le paiement a du être annulé car le montant de la transation doit être supérieur à 0");
        }
    }

    // Permet de sauvegarder un paiement via internet
    @PostMapping(value="/user/{userId}/transaction/saveInternetTransaction")
    @PreAuthorize("(hasRole('ROLE_USER') && hasPermission(#userId, 'User', 'MANAGE_USER')) || hasRole('ROLE_ADMIN')")
    public ResponseEntity<?> saveTransactionFromInternet(@PathVariable("userId") String userId, @RequestBody @Valid TransactionInternetInputFormat transactionVerified) throws ParseException {

        // On vérifie que me montant est bien supérieur à 0
        if (transactionVerified.getAmount() > 0) {
            // On vérifie si le compte qui va recevoir l'argent existe
            Optional<Account> account_target = accountServ.getAccountByIban(transactionVerified.getReceiver_account_iban());

            if (account_target.isPresent()) {

                Optional<Card> card = cardServ.getByCardNumberAndCCVAndExpirationDate(transactionVerified.getNum_card(), transactionVerified.getCcv_code(), transactionVerified.getExpiration_date());
                // On vérifie si la carte existe
                if (card != null && card.isPresent()) {

                    // On vérifie que le compte débiteur appartient bien à la personne qui effectue le virement
                    if (!card.get().getAccount().getOwner().getId().equals(userId)) {
                        return ErrorRequest.errorRequestMessage("L'id du compte débiteur ne correspond pas avec l'id insérer en paramètre");
                    }

                    // On vérifie que la carte est toujours valide
                    if (cardServ.isAlreadyValidCard(card.get())) {

                        // On vérifie que le montant de paiement ne dépasse pas le plafond de la carte et que l'utilisateur possède assez d'argent
                        if (card.get().getCeiling() >= transactionVerified.getAmount() && card.get().getAccount().getBalance() >= transactionVerified.getAmount()) {

                            // On vérifie si la carte n'as pas été supprimée
                            if (card.get().getDeleted_date().equals("")) {

                                // On vérifie que la carte n'est pas bloquée
                                if (!card.get().getBlocked()) {
                                    // On récupère la date current
                                    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                                    Date date = new Date();
                                    String currentDate = formatter.format(date).toString();

                                    // On récupère le taux à appliquer en fonction des pays d'origine des comptes
                                    String currency_debtor = AccountController.country_devise_ibanCode.get(card.get().getAccount().getCountry()).getDevise();
                                    String currency_target = AccountController.country_devise_ibanCode.get(account_target.get().getCountry()).getDevise();

                                    // On vérifie si les paiements à l'étranger sont possibles
                                    if (!card.get().getLocalisation() && !currency_debtor.equals(currency_target)) {
                                        return ErrorRequest.errorRequestMessage("Les paiements à l'étranger sont désactivés");
                                    }

                                    Double rateToApply = 1.0;
                                    if (!currency_debtor.equals(currency_target)) { // Si le paiement se fais entre deux compte qui ont des monnaies différentes alors on récupère le taux de change
                                        rateToApply = AccountController.exchange_rates.get(currency_debtor).get(currency_target);
                                    }

                                    Transaction newTransaction = transactionServ.saveTransaction(new Transaction(UUID.randomUUID().toString(), currentDate, transactionVerified.getLibelle(), transactionVerified.getAmount(), rateToApply, card.get(), account_target.get()));

                                    // Si tout s'est exécuté comme il faut alors on peut effectuer le paiement
                                    // On débite le compte
                                    card.get().getAccount().setBalance(card.get().getAccount().getBalance()-transactionVerified.getAmount());
                                    cardServ.saveCard(card.get());

                                    // On vérifie si il s'agit d'une carte virtuel, si c'est le cas alors on la supprime
                                    if (card.get().getIs_virtual_card()) {
                                        cardServ.deleteCard(card.get());
                                    }

                                    // Et enfin on crédite l'autre compte
                                    account_target.get().setBalance(account_target.get().getBalance()+(transactionVerified.getAmount()*rateToApply));
                                    accountServ.saveAccount(account_target.get());

                                    URI location = linkTo(methodOn(OperationController.class).getTransactionById(newTransaction.getId())).toUri();
                                    return ResponseEntity.created(location).body(ResponseEntity.ok(transactionViewAssembler.toModel(newTransaction)));
                                } else {
                                    return ErrorRequest.errorRequestMessage("La carte est bloquée");
                                }
                            } else {
                                return ErrorRequest.errorRequestMessage("La carte est supprimée et par conséquent inutilisable");
                            }
                        } else {
                            return ErrorRequest.errorRequestMessage("Le montant dépasse le plafond de la carte");
                        }
                    } else {
                        // On supprime la carte car elle n'est plus valide
                        cardServ.deleteCard(card.get());

                        return ErrorRequest.errorRequestMessage("La carte renseignée n'est plus valide");
                    }
                } else {
                    return ErrorRequest.errorRequestMessage("La carte renseignée n'existe pas das le system, merci de vérifier vos informations");
                }
            } else {
                return ErrorRequest.errorRequestMessage("Le paiement a du être annulé car le compte recevant l'argent identidié par l'iban : " + transactionVerified.getReceiver_account_iban() + " n'existe pas");
            }
        } else {
            return ErrorRequest.errorRequestMessage("Le paiement a du être annulé car le montant de la transation doit être supérieur à 0");
        }
    }
}
