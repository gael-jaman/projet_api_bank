package fr.ul.miage.projet.API.assembler;

import fr.ul.miage.projet.API.controller.CardController;
import fr.ul.miage.projet.API.entity.Card;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.springframework.hateoas.server.core.DummyInvocationUtils.methodOn;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

@Component
public class CardAssembler implements RepresentationModelAssembler<Card, EntityModel<Card>> {
    @Override
    public EntityModel<Card> toModel(Card card) {
        return EntityModel.of(card,
                linkTo(methodOn(CardController.class).getCardById(card.getNum_card())).withSelfRel(),
                linkTo(methodOn(CardController.class).getAllCards()).withRel("collection"));
    }

    public CollectionModel<EntityModel<Card>> toCollectionModel(Iterable<? extends Card> entities) {
        List<EntityModel<Card>> cardModel = StreamSupport
                .stream(entities.spliterator(), false)
                .map(i -> toModel(i))
                .collect(Collectors.toList());
        return CollectionModel.of(cardModel,
                linkTo(methodOn(CardController.class)
                        .getAllCards()).withSelfRel());
    }
}
