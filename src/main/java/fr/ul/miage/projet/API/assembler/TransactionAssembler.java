package fr.ul.miage.projet.API.assembler;

import fr.ul.miage.projet.API.controller.OperationController;
import fr.ul.miage.projet.API.entity.Transaction;
import fr.ul.miage.projet.API.entity.Transfer;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.springframework.hateoas.server.core.DummyInvocationUtils.methodOn;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

@Component
public class TransactionAssembler implements RepresentationModelAssembler<Transaction, EntityModel<Transaction>> {

    @Override
    public EntityModel<Transaction> toModel(Transaction transaction) {
        return EntityModel.of(transaction,
                linkTo(methodOn(OperationController.class).getTransactionById(transaction.getId())).withSelfRel(),
                linkTo(methodOn(OperationController.class).getAllTransactions()).withRel("collection"));
    }

    public CollectionModel<EntityModel<Transaction>> toCollectionModel(Iterable<? extends Transaction> entities) {
        List<EntityModel<Transaction>> transactionModel = StreamSupport
                .stream(entities.spliterator(), false)
                .map(i -> toModel(i))
                .collect(Collectors.toList());
        return CollectionModel.of(transactionModel,
                linkTo(methodOn(OperationController.class)
                        .getAllTransactions()).withSelfRel());
    }
}
