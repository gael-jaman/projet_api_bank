package fr.ul.miage.projet.API.service;

import fr.ul.miage.projet.API.entity.Transaction;

import java.util.List;
import java.util.Optional;

public interface TransactionService {

    // Permet de sauvegarder une transactions
    Transaction saveTransaction(Transaction transaction);

    // Permet de récupérer la transaction à partir de son id
    Optional<Transaction> getById(String id);

    // Permet de récupérer les transactions à partir de l'id de la carte
    List<Transaction> getTransactionsByCardNumber(String cardNumber);

    List<Transaction> getTransactionsByAccountIban(String iban);

    // Permet de récupérer tous les transactions
    List<Transaction> getAllTransactions();

}
