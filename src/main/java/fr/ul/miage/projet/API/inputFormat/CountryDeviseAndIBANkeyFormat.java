package fr.ul.miage.projet.API.inputFormat;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CountryDeviseAndIBANkeyFormat {

    private String devise;

    private String ibanCode;
}
