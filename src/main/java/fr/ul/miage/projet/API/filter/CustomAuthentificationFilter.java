package fr.ul.miage.projet.API.filter;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
public class CustomAuthentificationFilter extends UsernamePasswordAuthenticationFilter {

    private final AuthenticationManager authenticationManager;

    public CustomAuthentificationFilter(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) {
        String userEmail = request.getParameter("email");
        String userPassword = request.getParameter("password");
        UsernamePasswordAuthenticationToken authenticationToken = null;

        // Si l'identification est un succès alors on appelle la function successfulAuthentication.
        // Sinon l'exception sera levée.
        try {
            authenticationToken =  new UsernamePasswordAuthenticationToken(userEmail, userPassword);
        } catch (AuthenticationException e) {
            log.info("Erreur détectée: " + e);
        }

        System.out.println(authenticationToken);

        return authenticationManager.authenticate(authenticationToken);
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authentication) throws IOException, ServletException {
        System.out.println("Authentificated");
        User user = (User) authentication.getPrincipal();
        Algorithm algorithm = Algorithm.HMAC256("secret".getBytes());
        String access_token = JWT.create()
                .withSubject(user.getUsername()) // Il s'agit ici de l'email de l'utilisateur
                .withExpiresAt(new Date(System.currentTimeMillis() + 10 * 60 * 1000)) // Ici on crée un token qui aura 10 min de validité
                .withIssuer(request.getRequestURL().toString())
                .withClaim("roles", user.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList()))
                .sign(algorithm);

        String refresh_token = JWT.create()
                .withSubject(user.getUsername()) // Il s'agit ici de l'email de l'utilisateur
                .withExpiresAt(new Date(System.currentTimeMillis() + 60 * 60 * 1000)) // Ici on crée un token qui aura 1 heure de validité
                .withIssuer(request.getRequestURL().toString())
                .sign(algorithm);

        Map<String, String> list_token = new HashMap<>();
        list_token.put("access_token", access_token);
        list_token.put("refresh_token", refresh_token);
        response.setContentType(APPLICATION_JSON_VALUE);
        new ObjectMapper().writeValue(response.getOutputStream(), list_token);
    }

}
