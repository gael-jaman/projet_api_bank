package fr.ul.miage.projet.API.entity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Card implements Serializable {

    private static final long serialVersionUID = 8241682591L;

    @Id
    @Column(name = "num_card", nullable = false)
    private String num_card; // Numéro de la carte bancaire
    private String secret_code; // Code secret de la carte bancaire
    private String visual_cryptogram; // Code CCV visible au dos de la carte
    private String expiration_date; // Date d'expiration de la carte
    private Boolean blocked; // on définit si la carte peut effectuer des opérations ou non
    private Boolean localisation; // on définit si la carte peut ou non effectuer des opérations en dehors du périmètre compatible avec la localisation du téléphone
    private Double ceiling; // Plafond du compte
    private Boolean without_contact; // Permet d'activer ou non le paiement sans contact
    private Boolean is_virtual_card; // L'utilisateur peut créer une carte virtuel pour effectuer des paiements sur internet
    @ManyToOne // Le premier est l'entité ou on se situe et le deuxième
    @JoinColumn(name = "account_iban")
    private Account account; // Compte lié à la carte
    private String deleted_date;

    public Card(String num_card, String secret_code, String visual_cryptogram, String expiration_date, Boolean blocked, Boolean localisation, Double ceiling, Boolean without_contact, Boolean is_virtual_card, Account account, String deleted_date) {
        this.num_card = num_card;
        this.secret_code = secret_code;
        this.visual_cryptogram = visual_cryptogram;
        this.expiration_date = expiration_date;
        this.blocked = blocked;
        this.localisation = localisation;
        this.ceiling = ceiling;
        this.without_contact = without_contact;
        this.is_virtual_card = is_virtual_card;
        this.account = account;
        this.deleted_date = deleted_date;
    }
}