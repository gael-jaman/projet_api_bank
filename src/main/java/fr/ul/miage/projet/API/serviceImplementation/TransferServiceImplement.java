package fr.ul.miage.projet.API.serviceImplementation;

import fr.ul.miage.projet.API.entity.Card;
import fr.ul.miage.projet.API.entity.Transfer;
import fr.ul.miage.projet.API.repository.TransferRepository;
import fr.ul.miage.projet.API.service.TransferService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Transactional
@Slf4j
public class TransferServiceImplement implements TransferService {

    private final TransferRepository transferRepo;

    @Override
    public Transfer saveTransfer(Transfer transfer) {
        log.info("Sauvegarde d'un nouveau virement ayant l'id suivant : " + transfer.getId());
        return transferRepo.save(transfer);
    }

    @Override
    public Optional<Transfer> getById(String id) {
        log.info("On récupère le virement ayant l'id suivant : " + id);
        return transferRepo.findById(id);
    }

    @Override
    public List<Transfer> getTransfersByAccountIban(String iban) {
        log.info("On récupère tous les virements correspondant au compte suivant : " + iban);
        List<Transfer> listTransfers = transferRepo.findAll();

        ArrayList<Transfer> transfersToShow = new ArrayList<Transfer>();
        for (Transfer transfer : listTransfers) {
            if ((transfer.getDebtor_account().getIban().equals(iban)) || (transfer.getTarget_account().getIban().equals(iban))) {
                transfersToShow.add(transfer);
            }
        }

        return transfersToShow;
    }

    @Override
    public List<Transfer> getAllTransfers() {
        log.info("On récupère tous les virements");
        return transferRepo.findAll();
    }
}
