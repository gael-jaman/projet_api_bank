package fr.ul.miage.projet.API.serviceImplementation;

import fr.ul.miage.projet.API.entity.Transfer;
import fr.ul.miage.projet.API.entity.User;
import fr.ul.miage.projet.API.entity.Role;
import fr.ul.miage.projet.API.repository.UserRepository;
import fr.ul.miage.projet.API.repository.RoleRepository;
import fr.ul.miage.projet.API.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;

@Service
@RequiredArgsConstructor
@Transactional
@Slf4j
public class UserServiceImplement implements UserService, UserDetailsService {

    private final UserRepository userRepo;
    private final RoleRepository roleRepo;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Optional<User> ckeckUser = userRepo.findByEmail(email);

        // On check si l'utilisateur est disponible dans la base de donnée
        if (ckeckUser.isPresent()) {
            log.info("Utilisateur trouvé dans la base de donnée: {}", email);
        } else {
            log.error("Utilisateur introuvable dans la base de données");
            throw new UsernameNotFoundException("Utilisateur introuvable dans la base de données");
        }

        // Si l'exception ne s'est pas déclenchée alors on récupère l'utilisateur
        User user = ckeckUser.get();

        // On souhaite récupérer les différents rôles de l'utilisateur
        Collection<SimpleGrantedAuthority> authorities = new ArrayList<>();
        user.getRoles().forEach(role -> {
            authorities.add(new SimpleGrantedAuthority(role.getName()));
        });

        System.out.println(authorities);

        // On passe en paramètre l'email de l'utilisateur et son mots de pass pour permettre l'identification.
        // Ensuite on donne ses rôles pour vérifié si cet utilisateur possède les droits nécessaires.
        return new org.springframework.security.core.userdetails.User(user.getEmail(), user.getPassword(), authorities);
    }

    @Override
    public User saveUser(User user) {
        log.info("Sauvegarde d'un nouvel utilisateur: " + user.getEmail());
        return userRepo.save(user);
    }

    @Override
    public Role saveRole(Role role) {
        log.info("Sauvegarde d'un nouveau role: " + role.getName() + role.getId());
        return roleRepo.save(role);
    }

    @Override
    public void addRoleToUser(String roleName, String userEmail) {
        User user = userRepo.findByEmail(userEmail).get();
        Role role = roleRepo.findByName(roleName);

        // On vérifie si l'utilisateur ne possède pas déjà ce rôle
        AtomicBoolean alreadyHasRole = new AtomicBoolean(false);
        user.getRoles().forEach(tmpRole -> {
            if (tmpRole.getName().equals(role.getName())) {
                alreadyHasRole.set(true);
            }
        });

        if (alreadyHasRole.get()) {
            log.info("L'utilisateur " + user.getEmail() + " possède déjà le role " + role.getName());
        } else {
            user.getRoles().add(role);
        }
    }

    @Override
    public Optional<User> getUserByEmail(String userEmail) {
        return userRepo.findByEmail(userEmail);
    }

    @Override
    public Optional<User> getUserById(String userId) {
        return userRepo.findById(userId);
    }

    @Override
    public List<User> getAllUsers() {
        return userRepo.findAll();
    }

    @Override
    public Boolean hasRole(User user, String role) {
        Collection<Role> listRoles = user.getRoles();

        Boolean didHasRole = false;
        for (Role roleElem : listRoles) {
            if (roleElem.getName().equals(role)) {
                System.out.println("role eleme: " + roleElem.getName());
                didHasRole = true;
                break;
            }
        }

        System.out.println("est admin: " + didHasRole);

        return didHasRole;
    }
}
