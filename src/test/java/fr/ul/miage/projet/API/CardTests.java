package fr.ul.miage.projet.API;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.ul.miage.projet.API.entity.Account;
import fr.ul.miage.projet.API.entity.Card;
import fr.ul.miage.projet.API.entity.User;
import fr.ul.miage.projet.API.inputFormat.AccountInputFormat;
import fr.ul.miage.projet.API.inputFormat.CardInputFormat;
import fr.ul.miage.projet.API.repository.*;
import fr.ul.miage.projet.API.service.AccountService;
import fr.ul.miage.projet.API.service.CardService;
import fr.ul.miage.projet.API.service.UserService;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.apache.http.HttpStatus;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.annotation.DirtiesContext;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.UUID;

import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class CardTests {

    @LocalServerPort
    int port;

    @Autowired
    UserRepository userRepo;

    @Autowired
    AccountRepository accountRepo;

    @Autowired
    CardRepository cardRepo;

    @Autowired
    RoleRepository roleRepo;

    @Autowired
    TransactionRepository transactionRepo;

    @Autowired
    TransferRepository transferRepo;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    UserService userServ;

    @Autowired
    AccountService accountServ;

    @Autowired
    CardService cardServ;

    @BeforeEach
    public void setupContext() {
        RestAssured.port = port;
        transactionRepo.deleteAll();
        transferRepo.deleteAll();
        cardRepo.deleteAll();
        accountRepo.deleteAll();
        userRepo.deleteAll();
    }

    @Test
    public void savePhysiqueCardTest() throws IOException, JSONException, URISyntaxException {

        User user = userServ.saveUser(generateUser("maxime@gmail.com"));
        userServ.addRoleToUser("ROLE_USER", user.getEmail());

        CardInputFormat cardInputFormat = new CardInputFormat(true, false);

        String access_token = getToken(user.getEmail(), "password");

        Account account = accountServ.saveAccount(new Account("FR124536724152675167281567", 0.0, "France", user));

        Response response = given()
                .header("Authorization", "Bearer " + access_token)
                .body(this.toJsonString(cardInputFormat))
                .contentType(ContentType.JSON)
                .when()
                .post("/cards/user/"+user.getId()+"/account/"+account.getIban()+"/saveCard")
                .then()
                .statusCode(HttpStatus.SC_CREATED)
                .extract()
                .response();

        String jsonAsString = response.asString();
        assertThat(jsonAsString, containsString(user.getId()));
    }

    @Test
    public void saveVirtualCardTest() throws IOException, JSONException, URISyntaxException {

        User user = userServ.saveUser(generateUser("maxime@gmail.com"));
        userServ.addRoleToUser("ROLE_USER", user.getEmail());

        CardInputFormat cardInputFormat = new CardInputFormat(true, true);

        String access_token = getToken(user.getEmail(), "password");

        Account account = accountServ.saveAccount(new Account("FR124536724152675167281567", 0.0, "France", user));

        Response response = given()
                .header("Authorization", "Bearer " + access_token)
                .body(this.toJsonString(cardInputFormat))
                .contentType(ContentType.JSON)
                .when()
                .post("/cards/user/"+user.getId()+"/account/"+account.getIban()+"/saveCard")
                .then()
                .statusCode(HttpStatus.SC_CREATED)
                .extract()
                .response();

        String jsonAsString = response.asString();
        assertThat(jsonAsString, containsString(user.getId()));
    }

    @Test
    public void getAllCardsTest() throws IOException, JSONException, URISyntaxException {

        User admin = userServ.saveUser(generateUser("admin@gmail.com"));
        userServ.addRoleToUser("ROLE_ADMIN", admin.getEmail());

        User user = userServ.saveUser(generateUser("maxime@gmail.com"));
        userServ.addRoleToUser("ROLE_USER", user.getEmail());

        Account account = accountServ.saveAccount(new Account("FR124536724152675167281567", 0.0, "France", user));

        cardServ.saveCard(new Card("4012001037141017", "1234", "123", "01/08/2025", false, true, 1500.0, true, false, account, ""));
        cardServ.saveCard(new Card("4012001037141018", "1234", "123", "01/08/2025", false, true, 1500.0, true, false, account, ""));
        cardServ.saveCard(new Card("4012001037141019", "1234", "123", "01/08/2025", false, true, 1500.0, true, false, account, ""));
        cardServ.saveCard(new Card("4012001037141010", "1234", "123", "01/08/2025", false, true, 1500.0, true, false, account, ""));

        String access_token = getToken(admin.getEmail(), "password");

        given()
                .header("Authorization", "Bearer " + access_token)
                .when()
                .get("/cards")
                .then()
                .statusCode(HttpStatus.SC_OK)
                .and()
                .assertThat()
                .body("_embedded.cards.size()", equalTo(4));
    }

    @Test
    public void getOneCardByIdTest() throws IOException, JSONException, URISyntaxException {

        User admin = userServ.saveUser(generateUser("admin@gmail.com"));
        userServ.addRoleToUser("ROLE_ADMIN", admin.getEmail());

        User user = userServ.saveUser(generateUser("maxime@gmail.com"));
        userServ.addRoleToUser("ROLE_USER", user.getEmail());

        Account account = accountServ.saveAccount(new Account("FR124536724152675167281567", 0.0, "France", user));

        Card card = cardServ.saveCard(new Card("4012001037141017", "1234", "123", "01/08/2025", false, true, 1500.0, true, false, account, ""));

        String access_token = getToken(admin.getEmail(), "password");

        Response response = given()
        .header("Authorization", "Bearer " + access_token)
        .when()
        .get("/cards/"+card.getNum_card())
        .then()
        .statusCode(HttpStatus.SC_OK)
        .extract()
        .response();

        String jsonAsString = response.asString();
        assertThat(jsonAsString, containsString(card.getNum_card()));
    }

    @Test
    public void lockCardTest() throws IOException, JSONException, URISyntaxException {

        User user = userServ.saveUser(generateUser("maxime@gmail.com"));
        userServ.addRoleToUser("ROLE_USER", user.getEmail());

        Account account = accountServ.saveAccount(new Account("FR124536724152675167281567", 0.0, "France", user));

        Card card = cardServ.saveCard(new Card("4012001037141017", "1234", "123", "01/08/2025", false, true, 1500.0, true, false, account, ""));

        String access_token = getToken(user.getEmail(), "password");

        given()
        .header("Authorization", "Bearer " + access_token)
        .when()
        .post("/cards/user/"+user.getId()+"/"+card.getNum_card()+"/lockCard")
        .then()
        .statusCode(HttpStatus.SC_OK)
        .and()
        .assertThat()
        .body("blocked", equalTo(true));
    }

    @Test
    public void unlockCardTest() throws IOException, JSONException, URISyntaxException {

        User user = userServ.saveUser(generateUser("maxime@gmail.com"));
        userServ.addRoleToUser("ROLE_USER", user.getEmail());

        Account account = accountServ.saveAccount(new Account("FR124536724152675167281567", 0.0, "France", user));

        Card card = cardServ.saveCard(new Card("4012001037141017", "1234", "123", "01/08/2025", true, true, 1500.0, true, false, account, ""));

        String access_token = getToken(user.getEmail(), "password");

        given()
        .header("Authorization", "Bearer " + access_token)
        .when()
        .post("/cards/user/"+user.getId()+"/"+card.getNum_card()+"/unlockCard")
        .then()
        .statusCode(HttpStatus.SC_OK)
        .and()
        .assertThat()
        .body("blocked", equalTo(false));
    }

    @Test
    public void disablePaiementInForeignCountrtCardTest() throws IOException, JSONException, URISyntaxException {

        User user = userServ.saveUser(generateUser("maxime@gmail.com"));
        userServ.addRoleToUser("ROLE_USER", user.getEmail());

        Account account = accountServ.saveAccount(new Account("FR124536724152675167281567", 0.0, "France", user));

        Card card = cardServ.saveCard(new Card("4012001037141017", "1234", "123", "01/08/2025", false, true, 1500.0, true, false, account, ""));

        String access_token = getToken(user.getEmail(), "password");

        given()
        .header("Authorization", "Bearer " + access_token)
        .when()
        .post("/cards/user/"+user.getId()+"/"+card.getNum_card()+"/disablePaiementInForeignCountry")
        .then()
        .statusCode(HttpStatus.SC_OK)
        .and()
        .assertThat()
        .body("localisation", equalTo(false));
    }

    @Test
    public void enablePaiementInForeignCountrtCardTest() throws IOException, JSONException, URISyntaxException {

        User user = userServ.saveUser(generateUser("maxime@gmail.com"));
        userServ.addRoleToUser("ROLE_USER", user.getEmail());

        Account account = accountServ.saveAccount(new Account("FR124536724152675167281567", 0.0, "France", user));

        Card card = cardServ.saveCard(new Card("4012001037141017", "1234", "123", "01/08/2025", false, false, 1500.0, true, false, account, ""));

        String access_token = getToken(user.getEmail(), "password");

        given()
        .header("Authorization", "Bearer " + access_token)
        .when()
        .post("/cards/user/"+user.getId()+"/"+card.getNum_card()+"/enablePaiementInForeignCountry")
        .then()
        .statusCode(HttpStatus.SC_OK)
        .and()
        .assertThat()
        .body("localisation", equalTo(true));
    }

    @Test
    public void disableWithoutContactCardTest() throws IOException, JSONException, URISyntaxException {

        User user = userServ.saveUser(generateUser("maxime@gmail.com"));
        userServ.addRoleToUser("ROLE_USER", user.getEmail());

        Account account = accountServ.saveAccount(new Account("FR124536724152675167281567", 0.0, "France", user));

        Card card = cardServ.saveCard(new Card("4012001037141017", "1234", "123", "01/08/2025", false, true, 1500.0, true, false, account, ""));

        String access_token = getToken(user.getEmail(), "password");

        given()
        .header("Authorization", "Bearer " + access_token)
        .when()
        .post("/cards/user/"+user.getId()+"/"+card.getNum_card()+"/disableWithoutContact")
        .then()
        .statusCode(HttpStatus.SC_OK)
        .and()
        .assertThat()
        .body("without_contact", equalTo(false));
    }

    @Test
    public void enableWithoutContactCardTest() throws IOException, JSONException, URISyntaxException {

        User user = userServ.saveUser(generateUser("maxime@gmail.com"));
        userServ.addRoleToUser("ROLE_USER", user.getEmail());

        Account account = accountServ.saveAccount(new Account("FR124536724152675167281567", 0.0, "France", user));

        Card card = cardServ.saveCard(new Card("4012001037141017", "1234", "123", "01/08/2025", false, true, 1500.0, false, false, account, ""));

        String access_token = getToken(user.getEmail(), "password");

        given()
        .header("Authorization", "Bearer " + access_token)
        .when()
        .post("/cards/user/"+user.getId()+"/"+card.getNum_card()+"/enableWithoutContact")
        .then()
        .statusCode(HttpStatus.SC_OK)
        .and()
        .assertThat()
        .body("without_contact", equalTo(true));
    }

    private String toJsonString(Object o) throws JsonProcessingException {
        ObjectMapper map = new ObjectMapper();
        return map.writeValueAsString(o);
    }

    private String getToken(String email, String password) throws JSONException, IOException, URISyntaxException {
        JSONObject json = new JSONObject();
        json.put("email", email);
        json.put("password", password);

        String json_body = String.format("email=%s&password=%s", email, "password");

        Response response = given()
                .with()
                .header("Content-Type", "application/x-www-form-urlencoded")
                .body(json_body)
                .when()
                .post("/login")
                .then()
                .extract()
                .response();

        String stringRes = response.asString();
        JSONObject jsonRes = new JSONObject(stringRes);
        return jsonRes.getString("access_token");
    }

    private User generateUser(String email) {
        return new User(UUID.randomUUID().toString(), "nomUser", "prenomUser", email, "ABCDEFGHIJK125", "0612345682", passwordEncoder.encode("password"), "12/08/1999");
    }
}
