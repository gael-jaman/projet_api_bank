package fr.ul.miage.projet.API;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.ul.miage.projet.API.entity.User;
import fr.ul.miage.projet.API.inputFormat.UserInputFormat;
import fr.ul.miage.projet.API.repository.*;
import fr.ul.miage.projet.API.service.UserService;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.apache.http.HttpStatus;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.annotation.DirtiesContext;

import java.io.IOException;
import java.net.*;
import java.util.UUID;

import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class UserTests {

    @LocalServerPort
    int port;

    @Autowired
    UserRepository userRepo;

    @Autowired
    AccountRepository accountRepo;

    @Autowired
    CardRepository cardRepo;

    @Autowired
    RoleRepository roleRepo;

    @Autowired
    TransactionRepository transactionRepo;

    @Autowired
    TransferRepository transferRepo;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    UserService userServ;

    @BeforeEach
    public void setupContext() {
        RestAssured.port = port;
        transactionRepo.deleteAll();
        transferRepo.deleteAll();
        cardRepo.deleteAll();
        accountRepo.deleteAll();
        userRepo.deleteAll();
    }

    @Test
    public void saveUserTest() throws IOException {

        UserInputFormat userInputFormat = new UserInputFormat("Maxime", "Marigliano", "max@gmail.com", "ABCDEFGHIJK125", "0671824519", "password", "10/12/1999");

        Response response = given()
        .header("Content-type", "application/json")
        .and()
        .body(this.toJsonString(userInputFormat))
        .contentType(ContentType.JSON)
        .when()
        .post("/users/save")
        .then()
        .statusCode(HttpStatus.SC_CREATED)
        .extract()
        .response();

        String jsonAsString = response.asString();
        assertThat(jsonAsString, containsString("max@gmail.com"));
    }

    @Test
    public void saveUserWithBadInputEmailTest() throws IOException {
        UserInputFormat userInputFormat = new UserInputFormat("Maxime", "Marigliano", "email", "ABCDEFGHIJK125", "0634256718", "password", "10/12/1999");
        given().header("Content-type", "application/json").and().body(this.toJsonString(userInputFormat)).contentType(ContentType.JSON).when().post("/users/save").then().statusCode(HttpStatus.SC_BAD_REQUEST).extract().response();
    }
    @Test
    public void saveUserWithBadInputNumPhoneTest() throws IOException {
        UserInputFormat userInputFormat = new UserInputFormat("Maxime", "Marigliano", "max@gmail.com", "ABCDEFGHIJK125", "", "password", "10/12/1999");
        given().header("Content-type", "application/json").and().body(this.toJsonString(userInputFormat)).contentType(ContentType.JSON).when().post("/users/save").then().statusCode(HttpStatus.SC_BAD_REQUEST).extract().response();
    }
    @Test
    public void saveUserWithBadInputBirthdayDateTest() throws IOException {
        UserInputFormat userInputFormat = new UserInputFormat("Maxime", "Marigliano", "max@gmail.com", "ABCDEFGHIJK125", "0671824519", "password", "10.12.1999");
        given().header("Content-type", "application/json").and().body(this.toJsonString(userInputFormat)).contentType(ContentType.JSON).when().post("/users/save").then().statusCode(HttpStatus.SC_BAD_REQUEST).extract().response();
    }

    @Test
    public void getOneUserTest() throws IOException, JSONException, URISyntaxException {

        User user = userServ.saveUser(generateUser("maxime@gmail.com"));

        userServ.addRoleToUser("ROLE_USER", user.getEmail());

        String access_token = getToken(user.getEmail(), "password");

        Response response = given()
                .header("Authorization", "Bearer " + access_token)
                .when().get("/users/"+user.getId())
                .then()
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .response();

        String jsonAsString = response.asString();
        assertThat(jsonAsString, containsString("maxime@gmail.com"));
    }

    @Test
    public void getAllUsersTest() throws IOException, JSONException, URISyntaxException {

        User user = userServ.saveUser(generateUser("maxime@gmail.com"));
        userServ.addRoleToUser("ROLE_ADMIN", user.getEmail());

        User user2 = userServ.saveUser(generateUser("maxime2@gmail.com"));

        User user3 = userServ.saveUser(generateUser("maxime3@gmail.com"));

        String access_token = getToken(user.getEmail(), "password");

        given()
        .header("Authorization", "Bearer " + access_token)
        .when().get("/users")
        .then()
        .statusCode(HttpStatus.SC_OK)
        .and()
        .assertThat()
        .body("_embedded.users.size()", equalTo(3));
    }

    private String toJsonString(Object o) throws JsonProcessingException {
        ObjectMapper map = new ObjectMapper();
        return map.writeValueAsString(o);
    }

    private String getToken(String email, String password) throws JSONException, IOException, URISyntaxException {
        JSONObject json = new JSONObject();
        json.put("email", email);
        json.put("password", password);

        String json_body = String.format("email=%s&password=%s", email, "password");

        Response response = given()
                .with()
                .header("Content-Type", "application/x-www-form-urlencoded")
                .body(json_body)
                .when()
                .post("/login")
                .then()
                .extract()
                .response();

        String stringRes = response.asString();
        JSONObject jsonRes = new JSONObject(stringRes);
        return jsonRes.getString("access_token");
    }

    private User generateUser(String email) {
        return new User(UUID.randomUUID().toString(), "nomUser", "prenomUser", email, "ABCDEFGHIJK125", "0612345682", passwordEncoder.encode("password"), "12/08/1999");
    }
}
