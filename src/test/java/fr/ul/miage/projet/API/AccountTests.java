package fr.ul.miage.projet.API;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.ul.miage.projet.API.entity.Account;
import fr.ul.miage.projet.API.entity.User;
import fr.ul.miage.projet.API.inputFormat.AccountInputFormat;
import fr.ul.miage.projet.API.repository.*;
import fr.ul.miage.projet.API.service.AccountService;
import fr.ul.miage.projet.API.service.UserService;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.apache.http.HttpStatus;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.annotation.DirtiesContext;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.UUID;

import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class AccountTests {

    @LocalServerPort
    int port;

    @Autowired
    UserRepository userRepo;

    @Autowired
    AccountRepository accountRepo;

    @Autowired
    CardRepository cardRepo;

    @Autowired
    RoleRepository roleRepo;

    @Autowired
    TransactionRepository transactionRepo;

    @Autowired
    TransferRepository transferRepo;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    UserService userServ;

    @Autowired
    AccountService accountServ;

    @BeforeEach
    public void setupContext() {
        RestAssured.port = port;
        transactionRepo.deleteAll();
        transferRepo.deleteAll();
        cardRepo.deleteAll();
        accountRepo.deleteAll();
        userRepo.deleteAll();
    }

    @Test
    public void saveAccountTest() throws IOException, JSONException, URISyntaxException {

        User user = userServ.saveUser(generateUser("maxime@gmail.com"));
        userServ.addRoleToUser("ROLE_USER", user.getEmail());

        AccountInputFormat accountInputFormat = new AccountInputFormat("Allemagne");

        String access_token = getToken(user.getEmail(), "password");

        Response response = given()
                .header("Authorization", "Bearer " + access_token)
                .body(this.toJsonString(accountInputFormat))
                .contentType(ContentType.JSON)
                .when()
                .post("/accounts/user/"+user.getId()+"/saveAccount")
                .then()
                .statusCode(HttpStatus.SC_CREATED)
                .extract()
                .response();

        String jsonAsString = response.asString();
        assertThat(jsonAsString, containsString(user.getId()));
    }

    @Test
    public void saveAccountWhenCountryIsNotInListTest() throws IOException, JSONException, URISyntaxException {

        User user = userServ.saveUser(generateUser("maxime@gmail.com"));
        userServ.addRoleToUser("ROLE_USER", user.getEmail());

        AccountInputFormat accountInputFormat = new AccountInputFormat("fauxPays");

        String access_token = getToken(user.getEmail(), "password");

        given()
        .header("Authorization", "Bearer " + access_token)
        .body(this.toJsonString(accountInputFormat))
        .contentType(ContentType.JSON)
        .when()
        .post("/accounts/user/"+user.getId()+"/saveAccount")
        .then()
        .statusCode(HttpStatus.SC_BAD_REQUEST)
        .extract()
        .response();
    }

    @Test
    public void getOneAccountByIbanTest() throws IOException, JSONException, URISyntaxException {

        User admin = userServ.saveUser(generateUser("admin@gmail.com"));
        userServ.addRoleToUser("ROLE_ADMIN", admin.getEmail());

        User user = userServ.saveUser(generateUser("maxime@gmail.com"));
        userServ.addRoleToUser("ROLE_USER", user.getEmail());

        String iban = "FR124536724152675167281567";
        Account account = accountServ.saveAccount(new Account(iban, 0.0, "France", user));

        String access_token = getToken(admin.getEmail(), "password");

        Response response = given()
                .header("Authorization", "Bearer " + access_token)
                .when()
                .get("/accounts/"+account.getIban())
                .then()
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .response();

        String jsonAsString = response.asString();
        assertThat(jsonAsString, containsString(iban));
    }

    @Test
    public void getOneAccountByIbanWhenIbanDoNotExistTest() throws IOException, JSONException, URISyntaxException {

        User admin = userServ.saveUser(generateUser("admin@gmail.com"));
        userServ.addRoleToUser("ROLE_ADMIN", admin.getEmail());

        User user = userServ.saveUser(generateUser("maxime@gmail.com"));
        userServ.addRoleToUser("ROLE_USER", user.getEmail());

        String iban = "FR124536724152675167281567";
        Account account = accountServ.saveAccount(new Account(iban, 0.0, "France", user));

        String access_token = getToken(admin.getEmail(), "password");

        // Cette personne n'as pas les droits d'accèder vers un autre iban
        given()
        .header("Authorization", "Bearer " + access_token)
        .when()
        .get("/accounts/356777656753")
        .then()
        .statusCode(HttpStatus.SC_BAD_REQUEST)
        .extract()
        .response();
    }

    @Test
    public void getAllAccountsTest() throws IOException, JSONException, URISyntaxException {

        User admin = userServ.saveUser(generateUser("admin@gmail.com"));
        userServ.addRoleToUser("ROLE_ADMIN", admin.getEmail());

        User user = userServ.saveUser(generateUser("maxime@gmail.com"));
        userServ.addRoleToUser("ROLE_ADMIN", user.getEmail());

        Account account1 = accountServ.saveAccount(new Account("FR124536724152675167281567", 0.0, "France", user));
        Account account2 = accountServ.saveAccount(new Account("FR124536724152675167281568", 0.0, "Allemagne", user));
        Account account3 = accountServ.saveAccount(new Account("FR124536724152675167281569", 0.0, "Espagne", user));
        Account account4 = accountServ.saveAccount(new Account("FR124536724152675167281561", 0.0, "Estonie", user));

        String access_token = getToken(admin.getEmail(), "password");

        given()
                .header("Authorization", "Bearer " + access_token)
                .when()
                .get("/accounts")
                .then()
                .statusCode(HttpStatus.SC_OK)
                .and()
                .assertThat()
                .body("_embedded.accounts.size()", equalTo(4));
    }

    @Test
    public void getAccountsByUserIdTest() throws IOException, JSONException, URISyntaxException {

        // Accounts user 1
        User user1 = userServ.saveUser(generateUser("user1@gmail.com"));
        userServ.addRoleToUser("ROLE_USER", user1.getEmail());
        accountServ.saveAccount(new Account("FR124536724152675167281567", 0.0, "France", user1));
        accountServ.saveAccount(new Account("FR124536724152675167281568", 0.0, "Allemagne", user1));

        // Accounts user 2
        User user2 = userServ.saveUser(generateUser("user2@gmail.com"));
        userServ.addRoleToUser("ROLE_USER", user2.getEmail());
        accountServ.saveAccount(new Account("FR124536724152675167281569", 0.0, "France", user2));

        String access_token = getToken(user1.getEmail(), "password");

        // On vérifie combien d'accounts possède user1
        given()
        .header("Authorization", "Bearer " + access_token)
        .when()
        .get("/accounts/user/"+user1.getId())
        .then()
        .statusCode(HttpStatus.SC_OK)
        .and()
        .assertThat()
        .body("_embedded.accounts.size()", equalTo(2));
    }

    private String toJsonString(Object o) throws JsonProcessingException {
        ObjectMapper map = new ObjectMapper();
        return map.writeValueAsString(o);
    }

    private String getToken(String email, String password) throws JSONException, IOException, URISyntaxException {
        JSONObject json = new JSONObject();
        json.put("email", email);
        json.put("password", password);

        String json_body = String.format("email=%s&password=%s", email, "password");

        Response response = given()
                .with()
                .header("Content-Type", "application/x-www-form-urlencoded")
                .body(json_body)
                .when()
                .post("/login")
                .then()
                .extract()
                .response();

        String stringRes = response.asString();
        JSONObject jsonRes = new JSONObject(stringRes);
        return jsonRes.getString("access_token");
    }

    private User generateUser(String email) {
        return new User(UUID.randomUUID().toString(), "nomUser", "prenomUser", email, "ABCDEFGHIJK125", "0612345682", passwordEncoder.encode("password"), "12/08/1999");
    }
}
