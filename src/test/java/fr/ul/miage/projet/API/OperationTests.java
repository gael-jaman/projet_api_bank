package fr.ul.miage.projet.API;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.ul.miage.projet.API.entity.*;
import fr.ul.miage.projet.API.inputFormat.*;
import fr.ul.miage.projet.API.repository.*;
import fr.ul.miage.projet.API.service.*;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.apache.http.HttpStatus;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.annotation.DirtiesContext;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.UUID;

import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class OperationTests {

    @LocalServerPort
    int port;

    @Autowired
    UserRepository userRepo;

    @Autowired
    AccountRepository accountRepo;

    @Autowired
    CardRepository cardRepo;

    @Autowired
    RoleRepository roleRepo;

    @Autowired
    TransactionRepository transactionRepo;

    @Autowired
    TransferRepository transferRepo;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    UserService userServ;

    @Autowired
    AccountService accountServ;

    @Autowired
    CardService cardServ;

    @Autowired
    TransferService transferServ;

    @Autowired
    TransactionService transactionServ;

    @BeforeEach
    public void setupContext() {
        RestAssured.port = port;
        transactionRepo.deleteAll();
        transferRepo.deleteAll();
        cardRepo.deleteAll();
        accountRepo.deleteAll();
        userRepo.deleteAll();
    }

    @Test
    public void saveTransferTest() throws IOException, JSONException, URISyntaxException {

        User user1 = userServ.saveUser(generateUser("user1@gmail.com"));
        userServ.addRoleToUser("ROLE_USER", user1.getEmail());
        Account account_user1 = accountServ.saveAccount(new Account("FR7612548029989876543210917", 30.0, "France", user1));

        User user2 = userServ.saveUser(generateUser("user2@gmail.com"));
        userServ.addRoleToUser("ROLE_USER", user2.getEmail());
        Account account_user2 = accountServ.saveAccount(new Account("FR7612548029989876543210910", 0.0, "France", user2));

        TransferInputFormat transferInputFormat = new TransferInputFormat("essais de transfer de 30€", 30.0, account_user1.getIban(), account_user2.getIban());

        String access_token = getToken(user1.getEmail(), "password");

        given()
        .header("Authorization", "Bearer " + access_token)
        .body(this.toJsonString(transferInputFormat))
        .contentType(ContentType.JSON)
        .when()
        .post("/operations/user/"+user1.getId()+"/transfer/save")
        .then()
        .statusCode(HttpStatus.SC_CREATED)
        .extract()
        .response();
    }

    @Test
    public void saveTransferWithWrongUserTest() throws IOException, JSONException, URISyntaxException {

        User user1 = userServ.saveUser(generateUser("user1@gmail.com"));
        userServ.addRoleToUser("ROLE_USER", user1.getEmail());
        Account account_user1 = accountServ.saveAccount(new Account("FR7612548029989876543210917", 30.0, "France", user1));

        User user2 = userServ.saveUser(generateUser("user2@gmail.com"));
        userServ.addRoleToUser("ROLE_USER", user2.getEmail());
        Account account_user2 = accountServ.saveAccount(new Account("FR7612548029989876543210910", 0.0, "France", user2));

        TransferInputFormat transferInputFormat = new TransferInputFormat("essais de transfer de 30€", 30.0, account_user1.getIban(), account_user2.getIban());

        String access_token = getToken(user1.getEmail(), "password");

        given()
        .header("Authorization", "Bearer " + access_token)
        .body(this.toJsonString(transferInputFormat))
        .contentType(ContentType.JSON)
        .when()
        .post("/operations/user/"+user2.getId()+"/transfer/save")
        .then()
        .statusCode(HttpStatus.SC_FORBIDDEN)
        .extract()
        .response();
    }

    @Test
    public void saveTransferWithTooMuchAmountTest() throws IOException, JSONException, URISyntaxException {

        User user1 = userServ.saveUser(generateUser("user1@gmail.com"));
        userServ.addRoleToUser("ROLE_USER", user1.getEmail());
        Account account_user1 = accountServ.saveAccount(new Account("FR7612548029989876543210917", 30.0, "France", user1));

        User user2 = userServ.saveUser(generateUser("user2@gmail.com"));
        userServ.addRoleToUser("ROLE_USER", user2.getEmail());
        Account account_user2 = accountServ.saveAccount(new Account("FR7612548029989876543210910", 0.0, "France", user2));

        TransferInputFormat transferInputFormat = new TransferInputFormat("essais de transfer de 30€", 40.0, account_user1.getIban(), account_user2.getIban());

        String access_token = getToken(user1.getEmail(), "password");

        given()
                .header("Authorization", "Bearer " + access_token)
                .body(this.toJsonString(transferInputFormat))
                .contentType(ContentType.JSON)
                .when()
                .post("/operations/user/"+user2.getId()+"/transfer/save")
                .then()
                .statusCode(HttpStatus.SC_FORBIDDEN)
                .extract()
                .response();
    }

    @Test
    public void getAllTransfersTest() throws IOException, JSONException, URISyntaxException {

        User admin = userServ.saveUser(generateUser("admin@gmail.com"));
        userServ.addRoleToUser("ROLE_ADMIN", admin.getEmail());

        User user1 = userServ.saveUser(generateUser("user1@gmail.com"));
        userServ.addRoleToUser("ROLE_USER", user1.getEmail());
        Account account_user1 = accountServ.saveAccount(new Account("FR7612548029989876543210917", 150.0, "France", user1));

        User user2 = userServ.saveUser(generateUser("user2@gmail.com"));
        userServ.addRoleToUser("ROLE_USER", user2.getEmail());
        Account account_user2 = accountServ.saveAccount(new Account("FR7612548029989876543210910", 0.0, "France", user2));

        transferServ.saveTransfer(new Transfer(UUID.randomUUID().toString(), "16/01/2022", "Virement de 10€", 10.0, 1.0, account_user1, account_user2));
        transferServ.saveTransfer(new Transfer(UUID.randomUUID().toString(), "16/01/2022", "Virement de 30€", 30.0, 1.0, account_user1, account_user2));

        String access_token = getToken(admin.getEmail(), "password");

        given()
        .header("Authorization", "Bearer " + access_token)
        .when()
        .get("/transfers")
        .then()
        .statusCode(HttpStatus.SC_OK)
        .and()
        .assertThat()
        .body("_embedded.transfers.size()", equalTo(2));
    }

    @Test
    public void getOneTransferByIdTest() throws IOException, JSONException, URISyntaxException {

        User admin = userServ.saveUser(generateUser("admin@gmail.com"));
        userServ.addRoleToUser("ROLE_ADMIN", admin.getEmail());

        User user1 = userServ.saveUser(generateUser("user1@gmail.com"));
        userServ.addRoleToUser("ROLE_USER", user1.getEmail());
        Account account_user1 = accountServ.saveAccount(new Account("FR7612548029989876543210917", 150.0, "France", user1));

        User user2 = userServ.saveUser(generateUser("user2@gmail.com"));
        userServ.addRoleToUser("ROLE_USER", user2.getEmail());
        Account account_user2 = accountServ.saveAccount(new Account("FR7612548029989876543210910", 0.0, "France", user2));

        Transfer transfer = transferServ.saveTransfer(new Transfer(UUID.randomUUID().toString(), "16/01/2022", "Virement de 10€", 10.0, 1.0, account_user1, account_user2));

        String access_token = getToken(admin.getEmail(), "password");

        Response response = given()
                .header("Authorization", "Bearer " + access_token)
                .when()
                .get("/operations/transfer/"+transfer.getId())
                .then()
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .response();

        String jsonAsString = response.asString();
        assertThat(jsonAsString, containsString(transfer.getId()));
    }

    @Test
    public void getOneTransfersByUserIdTest() throws IOException, JSONException, URISyntaxException {

        User user1 = userServ.saveUser(generateUser("user1@gmail.com"));
        userServ.addRoleToUser("ROLE_USER", user1.getEmail());
        Account account_user1 = accountServ.saveAccount(new Account("FR7612548029989876543210917", 150.0, "France", user1));

        User user2 = userServ.saveUser(generateUser("user2@gmail.com"));
        userServ.addRoleToUser("ROLE_USER", user2.getEmail());
        Account account_user2 = accountServ.saveAccount(new Account("FR7612548029989876543210910", 0.0, "France", user2));

        transferServ.saveTransfer(new Transfer(UUID.randomUUID().toString(), "16/01/2022", "Virement de 10€", 10.0, 1.0, account_user1, account_user2));
        transferServ.saveTransfer(new Transfer(UUID.randomUUID().toString(), "16/01/2022", "Virement de 5€", 5.0, 1.0, account_user1, account_user2));
        transferServ.saveTransfer(new Transfer(UUID.randomUUID().toString(), "16/01/2022", "Virement de 50€", 50.0, 1.0, account_user1, account_user2));

        String access_token = getToken(user1.getEmail(), "password");

        given()
        .header("Authorization", "Bearer " + access_token)
        .when()
        .get("/operations/transfer/user/"+user1.getId())
        .then()
        .statusCode(HttpStatus.SC_OK)
        .and()
        .assertThat()
        .body("_embedded.transferViews.size()", equalTo(3));
    }

    @Test
    public void depositMoneyToAccountTest() throws IOException, JSONException, URISyntaxException {

        User user = userServ.saveUser(generateUser("user1@gmail.com"));
        userServ.addRoleToUser("ROLE_USER", user.getEmail());
        Account account_user = accountServ.saveAccount(new Account("FR7612548029989876543210917", 0.0, "France", user));

        DepositAndWithdrawInputFormat depositAndWithdrawInputFormat = new DepositAndWithdrawInputFormat(100.0);

        String access_token = getToken(user.getEmail(), "password");

        given()
        .header("Authorization", "Bearer " + access_token)
        .body(this.toJsonString(depositAndWithdrawInputFormat))
        .contentType(ContentType.JSON)
        .when()
        .post("/operations/user/"+user.getId()+"/account/"+account_user.getIban()+"/deposit")
        .then()
        .statusCode(HttpStatus.SC_OK)
        .extract()
        .response();
    }

    @Test
    public void withdrawMoneyToAccountTest() throws IOException, JSONException, URISyntaxException {

        User user = userServ.saveUser(generateUser("user1@gmail.com"));
        userServ.addRoleToUser("ROLE_USER", user.getEmail());
        Account account_user = accountServ.saveAccount(new Account("FR7612548029989876543210917", 200.0, "France", user));

        DepositAndWithdrawInputFormat depositAndWithdrawInputFormat = new DepositAndWithdrawInputFormat(100.0);

        String access_token = getToken(user.getEmail(), "password");

        given()
        .header("Authorization", "Bearer " + access_token)
        .body(this.toJsonString(depositAndWithdrawInputFormat))
        .contentType(ContentType.JSON)
        .when()
        .post("/operations/user/"+user.getId()+"/account/"+account_user.getIban()+"/withdraw")
        .then()
        .statusCode(HttpStatus.SC_OK)
        .extract()
        .response();
    }

    @Test
    public void withdrawMoneyToAccountWhenNotEnoughMoneyTest() throws IOException, JSONException, URISyntaxException {

        User user = userServ.saveUser(generateUser("user1@gmail.com"));
        userServ.addRoleToUser("ROLE_USER", user.getEmail());
        Account account_user = accountServ.saveAccount(new Account("FR7612548029989876543210917", 0.0, "France", user));

        DepositAndWithdrawInputFormat depositAndWithdrawInputFormat = new DepositAndWithdrawInputFormat(100.0);

        String access_token = getToken(user.getEmail(), "password");

        given()
        .header("Authorization", "Bearer " + access_token)
        .body(this.toJsonString(depositAndWithdrawInputFormat))
        .contentType(ContentType.JSON)
        .when()
        .post("/operations/user/"+user.getId()+"/account/"+account_user.getIban()+"/withdraw")
        .then()
        .statusCode(HttpStatus.SC_BAD_REQUEST)
        .extract()
        .response();
    }

    @Test
    public void withdrawMoneyToAccountFromWrongUserTest() throws IOException, JSONException, URISyntaxException {

        User user = userServ.saveUser(generateUser("user1@gmail.com"));
        userServ.addRoleToUser("ROLE_USER", user.getEmail());
        Account account_user = accountServ.saveAccount(new Account("FR7612548029989876543210917", 200.0, "France", user));

        User user2 = userServ.saveUser(generateUser("user2@gmail.com"));
        userServ.addRoleToUser("ROLE_USER", user2.getEmail());

        DepositAndWithdrawInputFormat depositAndWithdrawInputFormat = new DepositAndWithdrawInputFormat(100.0);

        String access_token = getToken(user2.getEmail(), "password");

        given()
                .header("Authorization", "Bearer " + access_token)
                .body(this.toJsonString(depositAndWithdrawInputFormat))
                .contentType(ContentType.JSON)
                .when()
                .post("/operations/user/"+user.getId()+"/account/"+account_user.getIban()+"/withdraw")
                .then()
                .statusCode(HttpStatus.SC_FORBIDDEN)
                .extract()
                .response();
    }

    @Test
    public void saveTransactionTPETest() throws IOException, JSONException, URISyntaxException {

        User user1 = userServ.saveUser(generateUser("user1@gmail.com"));
        userServ.addRoleToUser("ROLE_USER", user1.getEmail());
        Account account_user1 = accountServ.saveAccount(new Account("FR7612548029989876543210917", 100.0, "France", user1));
        Card card = cardServ.saveCard(new Card("4012001037141017", "1234", "123", "01/08/2025", false, true, 1500.0, true, false, account_user1, ""));

        User user2 = userServ.saveUser(generateUser("user2@gmail.com"));
        userServ.addRoleToUser("ROLE_USER", user2.getEmail());
        Account account_user2 = accountServ.saveAccount(new Account("FR7612548029989876543210910", 0.0, "France", user2));

        TransactionTPEInputFormat transactionInputFormat = new TransactionTPEInputFormat("Transaction de 30€", 30.0, false, card.getNum_card(), card.getSecret_code(), account_user2.getIban());

        String access_token = getToken(user1.getEmail(), "password");

        given()
        .header("Authorization", "Bearer " + access_token)
        .body(this.toJsonString(transactionInputFormat))
        .contentType(ContentType.JSON)
        .when()
        .post("/operations/user/"+user1.getId()+"/transaction/saveTPEtransaction")
        .then()
        .statusCode(HttpStatus.SC_CREATED)
        .extract()
        .response();
    }

    @Test
    public void saveTransactionTPEWithoutContactTest() throws IOException, JSONException, URISyntaxException {

        User user1 = userServ.saveUser(generateUser("user1@gmail.com"));
        userServ.addRoleToUser("ROLE_USER", user1.getEmail());
        Account account_user1 = accountServ.saveAccount(new Account("FR7612548029989876543210917", 100.0, "France", user1));
        Card card = cardServ.saveCard(new Card("4012001037141017", "1234", "123", "01/08/2025", false, true, 1500.0, true, false, account_user1, ""));

        User user2 = userServ.saveUser(generateUser("user2@gmail.com"));
        userServ.addRoleToUser("ROLE_USER", user2.getEmail());
        Account account_user2 = accountServ.saveAccount(new Account("FR7612548029989876543210910", 0.0, "France", user2));

        TransactionTPEInputFormat transactionInputFormat = new TransactionTPEInputFormat("Transaction de 30€", 30.0, true, card.getNum_card(), card.getSecret_code(), account_user2.getIban());

        String access_token = getToken(user1.getEmail(), "password");

        given()
                .header("Authorization", "Bearer " + access_token)
                .body(this.toJsonString(transactionInputFormat))
                .contentType(ContentType.JSON)
                .when()
                .post("/operations/user/"+user1.getId()+"/transaction/saveTPEtransaction")
                .then()
                .statusCode(HttpStatus.SC_CREATED)
                .extract()
                .response();
    }

    @Test
    public void saveTransactionTPEWithTooMuchAmountTest() throws IOException, JSONException, URISyntaxException {

        User user1 = userServ.saveUser(generateUser("user1@gmail.com"));
        userServ.addRoleToUser("ROLE_USER", user1.getEmail());
        Account account_user1 = accountServ.saveAccount(new Account("FR7612548029989876543210917", 100.0, "France", user1));
        Card card = cardServ.saveCard(new Card("4012001037141017", "1234", "123", "01/08/2025", false, true, 1500.0, true, false, account_user1, ""));

        User user2 = userServ.saveUser(generateUser("user2@gmail.com"));
        userServ.addRoleToUser("ROLE_USER", user2.getEmail());
        Account account_user2 = accountServ.saveAccount(new Account("FR7612548029989876543210910", 0.0, "France", user2));

        TransactionTPEInputFormat transactionInputFormat = new TransactionTPEInputFormat("Transaction de 200€", 200.0, false, card.getNum_card(), card.getSecret_code(), account_user2.getIban());

        String access_token = getToken(user1.getEmail(), "password");

        given()
                .header("Authorization", "Bearer " + access_token)
                .body(this.toJsonString(transactionInputFormat))
                .contentType(ContentType.JSON)
                .when()
                .post("/operations/user/"+user1.getId()+"/transaction/saveTPEtransaction")
                .then()
                .statusCode(HttpStatus.SC_BAD_REQUEST)
                .extract()
                .response();
    }

    @Test
    public void saveTransactionTPEFromWrongUserTest() throws IOException, JSONException, URISyntaxException {

        User user1 = userServ.saveUser(generateUser("user1@gmail.com"));
        userServ.addRoleToUser("ROLE_USER", user1.getEmail());
        Account account_user1 = accountServ.saveAccount(new Account("FR7612548029989876543210917", 100.0, "France", user1));
        Card card = cardServ.saveCard(new Card("4012001037141017", "1234", "123", "01/08/2025", false, true, 1500.0, true, false, account_user1, ""));

        User user2 = userServ.saveUser(generateUser("user2@gmail.com"));
        userServ.addRoleToUser("ROLE_USER", user2.getEmail());
        Account account_user2 = accountServ.saveAccount(new Account("FR7612548029989876543210910", 0.0, "France", user2));

        TransactionTPEInputFormat transactionInputFormat = new TransactionTPEInputFormat("Transaction de 30€", 30.0, false, card.getNum_card(), card.getSecret_code(), account_user2.getIban());

        String access_token = getToken(user2.getEmail(), "password");

        given()
        .header("Authorization", "Bearer " + access_token)
        .body(this.toJsonString(transactionInputFormat))
        .contentType(ContentType.JSON)
        .when()
        .post("/operations/user/"+user1.getId()+"/transaction/saveTPEtransaction")
        .then()
        .statusCode(HttpStatus.SC_FORBIDDEN)
        .extract()
        .response();
    }

    @Test
    public void saveTransactionTPEWithWrongNumCardTest() throws IOException, JSONException, URISyntaxException {

        User user1 = userServ.saveUser(generateUser("user1@gmail.com"));
        userServ.addRoleToUser("ROLE_USER", user1.getEmail());
        Account account_user1 = accountServ.saveAccount(new Account("FR7612548029989876543210917", 100.0, "France", user1));
        Card card = cardServ.saveCard(new Card("4012001037141017", "1234", "123", "01/08/2025", false, true, 1500.0, true, false, account_user1, ""));

        User user2 = userServ.saveUser(generateUser("user2@gmail.com"));
        userServ.addRoleToUser("ROLE_USER", user2.getEmail());
        Account account_user2 = accountServ.saveAccount(new Account("FR7612548029989876543210910", 0.0, "France", user2));

        TransactionTPEInputFormat transactionInputFormat = new TransactionTPEInputFormat("Transaction de 30€", 30.0, false, "", card.getSecret_code(), account_user2.getIban());

        String access_token = getToken(user2.getEmail(), "password");

        given()
        .header("Authorization", "Bearer " + access_token)
        .body(this.toJsonString(transactionInputFormat))
        .contentType(ContentType.JSON)
        .when()
        .post("/operations/user/"+user1.getId()+"/transaction/saveTPEtransaction")
        .then()
        .statusCode(HttpStatus.SC_BAD_REQUEST)
        .extract()
        .response();
    }

    @Test
    public void saveTransactionTPEWithWrongSecretCodeTest() throws IOException, JSONException, URISyntaxException {

        User user1 = userServ.saveUser(generateUser("user1@gmail.com"));
        userServ.addRoleToUser("ROLE_USER", user1.getEmail());
        Account account_user1 = accountServ.saveAccount(new Account("FR7612548029989876543210917", 100.0, "France", user1));
        Card card = cardServ.saveCard(new Card("4012001037141017", "1234", "123", "01/08/2025", false, true, 1500.0, true, false, account_user1, ""));

        User user2 = userServ.saveUser(generateUser("user2@gmail.com"));
        userServ.addRoleToUser("ROLE_USER", user2.getEmail());
        Account account_user2 = accountServ.saveAccount(new Account("FR7612548029989876543210910", 0.0, "France", user2));

        TransactionTPEInputFormat transactionInputFormat = new TransactionTPEInputFormat("Transaction de 30€", 30.0, false, card.getNum_card(), "", account_user2.getIban());

        String access_token = getToken(user2.getEmail(), "password");

        given()
        .header("Authorization", "Bearer " + access_token)
        .body(this.toJsonString(transactionInputFormat))
        .contentType(ContentType.JSON)
        .when()
        .post("/operations/user/"+user1.getId()+"/transaction/saveTPEtransaction")
        .then()
        .statusCode(HttpStatus.SC_BAD_REQUEST)
        .extract()
        .response();
    }

    @Test
    public void saveTransactionInternetTest() throws IOException, JSONException, URISyntaxException {
        User user1 = userServ.saveUser(generateUser("user1@gmail.com"));
        userServ.addRoleToUser("ROLE_USER", user1.getEmail());
        Account account_user1 = accountServ.saveAccount(new Account("FR7612548029989876543210917", 100.0, "France", user1));
        Card card = cardServ.saveCard(new Card("4012001037141017", "1234", "123", "01/08/2025", false, true, 1500.0, true, false, account_user1, ""));

        User user2 = userServ.saveUser(generateUser("user2@gmail.com"));
        userServ.addRoleToUser("ROLE_USER", user2.getEmail());
        Account account_user2 = accountServ.saveAccount(new Account("FR7612548029989876543210910", 0.0, "France", user2));

        TransactionInternetInputFormat transactionInputFormat = new TransactionInternetInputFormat("Transaction de 30€", 30.0, card.getNum_card(), card.getVisual_cryptogram(), card.getExpiration_date(), account_user2.getIban());

        String access_token = getToken(user1.getEmail(), "password");

        given()
        .header("Authorization", "Bearer " + access_token)
        .body(this.toJsonString(transactionInputFormat))
        .contentType(ContentType.JSON)
        .when()
        .post("/operations/user/"+user1.getId()+"/transaction/saveInternetTransaction")
        .then()
        .statusCode(HttpStatus.SC_CREATED)
        .extract()
        .response();
    }

    @Test
    public void saveTransactionInternetWhenDigitalCardTest() throws IOException, JSONException, URISyntaxException {
        User user1 = userServ.saveUser(generateUser("user1@gmail.com"));
        userServ.addRoleToUser("ROLE_USER", user1.getEmail());
        Account account_user1 = accountServ.saveAccount(new Account("FR7612548029989876543210917", 100.0, "France", user1));
        Card card = cardServ.saveCard(new Card("4012001037141017", "1234", "123", "01/08/2025", false, true, 1500.0, true, true, account_user1, ""));

        User user2 = userServ.saveUser(generateUser("user2@gmail.com"));
        userServ.addRoleToUser("ROLE_USER", user2.getEmail());
        Account account_user2 = accountServ.saveAccount(new Account("FR7612548029989876543210910", 0.0, "France", user2));

        TransactionInternetInputFormat transactionInputFormat = new TransactionInternetInputFormat("Transaction de 30€", 30.0, card.getNum_card(), card.getVisual_cryptogram(), card.getExpiration_date(), account_user2.getIban());

        String access_token = getToken(user1.getEmail(), "password");

        given()
                .header("Authorization", "Bearer " + access_token)
                .body(this.toJsonString(transactionInputFormat))
                .contentType(ContentType.JSON)
                .when()
                .post("/operations/user/"+user1.getId()+"/transaction/saveInternetTransaction")
                .then()
                .statusCode(HttpStatus.SC_CREATED)
                .extract()
                .response();
    }

    @Test
    public void saveTransactionInternetFromWhrongUserTest() throws IOException, JSONException, URISyntaxException {

        User user1 = userServ.saveUser(generateUser("user1@gmail.com"));
        userServ.addRoleToUser("ROLE_USER", user1.getEmail());
        Account account_user1 = accountServ.saveAccount(new Account("FR7612548029989876543210917", 100.0, "France", user1));
        Card card = cardServ.saveCard(new Card("4012001037141017", "1234", "123", "01/08/2025", false, true, 1500.0, true, false, account_user1, ""));

        User user2 = userServ.saveUser(generateUser("user2@gmail.com"));
        userServ.addRoleToUser("ROLE_USER", user2.getEmail());
        Account account_user2 = accountServ.saveAccount(new Account("FR7612548029989876543210910", 0.0, "France", user2));

        TransactionInternetInputFormat transactionInputFormat = new TransactionInternetInputFormat("Transaction de 30€", 30.0, card.getNum_card(), card.getVisual_cryptogram(), card.getExpiration_date(), account_user2.getIban());

        String access_token = getToken(user2.getEmail(), "password");

        given()
        .header("Authorization", "Bearer " + access_token)
        .body(this.toJsonString(transactionInputFormat))
        .contentType(ContentType.JSON)
        .when()
        .post("/operations/user/"+user1.getId()+"/transaction/saveInternetTransaction")
        .then()
        .statusCode(HttpStatus.SC_FORBIDDEN)
        .extract()
        .response();
    }

    @Test
    public void saveTransactionInternetWithTooMuchAmountTest() throws IOException, JSONException, URISyntaxException {
        User user1 = userServ.saveUser(generateUser("user1@gmail.com"));
        userServ.addRoleToUser("ROLE_USER", user1.getEmail());
        Account account_user1 = accountServ.saveAccount(new Account("FR7612548029989876543210917", 100.0, "France", user1));
        Card card = cardServ.saveCard(new Card("4012001037141017", "1234", "123", "01/08/2025", false, true, 1500.0, true, false, account_user1, ""));

        User user2 = userServ.saveUser(generateUser("user2@gmail.com"));
        userServ.addRoleToUser("ROLE_USER", user2.getEmail());
        Account account_user2 = accountServ.saveAccount(new Account("FR7612548029989876543210910", 0.0, "France", user2));

        TransactionInternetInputFormat transactionInputFormat = new TransactionInternetInputFormat("Transaction de 200€",  200.0, card.getNum_card(), card.getVisual_cryptogram(), card.getExpiration_date(), account_user2.getIban());

        String access_token = getToken(user1.getEmail(), "password");

        given()
        .header("Authorization", "Bearer " + access_token)
        .body(this.toJsonString(transactionInputFormat))
        .contentType(ContentType.JSON)
        .when()
        .post("/operations/user/"+user1.getId()+"/transaction/saveInternetTransaction")
        .then()
        .statusCode(HttpStatus.SC_BAD_REQUEST)
        .extract()
        .response();
    }

    @Test
    public void saveTransactionInternetWithWrongNumCardTest() throws IOException, JSONException, URISyntaxException {

        User user1 = userServ.saveUser(generateUser("user1@gmail.com"));
        userServ.addRoleToUser("ROLE_USER", user1.getEmail());
        Account account_user1 = accountServ.saveAccount(new Account("FR7612548029989876543210917", 100.0, "France", user1));
        Card card = cardServ.saveCard(new Card("4012001037141017", "1234", "123", "01/08/2025", false, true, 1500.0, true, false, account_user1, ""));

        User user2 = userServ.saveUser(generateUser("user2@gmail.com"));
        userServ.addRoleToUser("ROLE_USER", user2.getEmail());
        Account account_user2 = accountServ.saveAccount(new Account("FR7612548029989876543210910", 0.0, "France", user2));

        TransactionInternetInputFormat transactionInputFormat = new TransactionInternetInputFormat("Transaction de 30€", 30.0, "", card.getVisual_cryptogram(), card.getExpiration_date(), account_user2.getIban());

        String access_token = getToken(user2.getEmail(), "password");

        given()
        .header("Authorization", "Bearer " + access_token)
        .body(this.toJsonString(transactionInputFormat))
        .contentType(ContentType.JSON)
        .when()
        .post("/operations/user/"+user1.getId()+"/transaction/saveInternetTransaction")
        .then()
        .statusCode(HttpStatus.SC_BAD_REQUEST)
        .extract()
        .response();
    }

    @Test
    public void saveTransactionInternetWithWrongCCVcodeTest() throws IOException, JSONException, URISyntaxException {

        User user1 = userServ.saveUser(generateUser("user1@gmail.com"));
        userServ.addRoleToUser("ROLE_USER", user1.getEmail());
        Account account_user1 = accountServ.saveAccount(new Account("FR7612548029989876543210917", 100.0, "France", user1));
        Card card = cardServ.saveCard(new Card("4012001037141017", "1234", "123", "01/08/2025", false, true, 1500.0, true, false, account_user1, ""));

        User user2 = userServ.saveUser(generateUser("user2@gmail.com"));
        userServ.addRoleToUser("ROLE_USER", user2.getEmail());
        Account account_user2 = accountServ.saveAccount(new Account("FR7612548029989876543210910", 0.0, "France", user2));

        TransactionInternetInputFormat transactionInputFormat = new TransactionInternetInputFormat("Transaction de 30€", 30.0, card.getNum_card(), "", card.getExpiration_date(), account_user2.getIban());

        String access_token = getToken(user2.getEmail(), "password");

        given()
        .header("Authorization", "Bearer " + access_token)
        .body(this.toJsonString(transactionInputFormat))
        .contentType(ContentType.JSON)
        .when()
        .post("/operations/user/"+user1.getId()+"/transaction/saveInternetTransaction")
        .then()
        .statusCode(HttpStatus.SC_BAD_REQUEST)
        .extract()
        .response();
    }

    @Test
    public void saveTransactionInternetWithWrongExperirationDateTest() throws IOException, JSONException, URISyntaxException {

        User user1 = userServ.saveUser(generateUser("user1@gmail.com"));
        userServ.addRoleToUser("ROLE_USER", user1.getEmail());
        Account account_user1 = accountServ.saveAccount(new Account("FR7612548029989876543210917", 100.0, "France", user1));
        Card card = cardServ.saveCard(new Card("4012001037141017", "1234", "123", "01/08/2025", false, true, 1500.0, true, false, account_user1, ""));

        User user2 = userServ.saveUser(generateUser("user2@gmail.com"));
        userServ.addRoleToUser("ROLE_USER", user2.getEmail());
        Account account_user2 = accountServ.saveAccount(new Account("FR7612548029989876543210910", 0.0, "France", user2));

        TransactionInternetInputFormat transactionInputFormat = new TransactionInternetInputFormat("Transaction de 30€", 30.0, card.getNum_card(), card.getVisual_cryptogram(), "", account_user2.getIban());

        String access_token = getToken(user2.getEmail(), "password");

        given()
        .header("Authorization", "Bearer " + access_token)
        .body(this.toJsonString(transactionInputFormat))
        .contentType(ContentType.JSON)
        .when()
        .post("/operations/user/"+user1.getId()+"/transaction/saveInternetTransaction")
        .then()
        .statusCode(HttpStatus.SC_BAD_REQUEST)
        .extract()
        .response();
    }

    @Test
    public void saveTransactionInternetWhenIsBlockedCardTest() throws IOException, JSONException, URISyntaxException {
        User user1 = userServ.saveUser(generateUser("user1@gmail.com"));
        userServ.addRoleToUser("ROLE_USER", user1.getEmail());
        Account account_user1 = accountServ.saveAccount(new Account("FR7612548029989876543210917", 100.0, "France", user1));
        Card card = cardServ.saveCard(new Card("4012001037141017", "1234", "123", "01/08/2025", true, true, 1500.0, true, false, account_user1, ""));

        User user2 = userServ.saveUser(generateUser("user2@gmail.com"));
        userServ.addRoleToUser("ROLE_USER", user2.getEmail());
        Account account_user2 = accountServ.saveAccount(new Account("FR7612548029989876543210910", 0.0, "France", user2));

        TransactionInternetInputFormat transactionInputFormat = new TransactionInternetInputFormat("Transaction de 30€", 30.0, card.getNum_card(), card.getVisual_cryptogram(), card.getExpiration_date(), account_user2.getIban());

        String access_token = getToken(user1.getEmail(), "password");

        given()
                .header("Authorization", "Bearer " + access_token)
                .body(this.toJsonString(transactionInputFormat))
                .contentType(ContentType.JSON)
                .when()
                .post("/operations/user/"+user1.getId()+"/transaction/saveInternetTransaction")
                .then()
                .statusCode(HttpStatus.SC_BAD_REQUEST)
                .extract()
                .response();
    }

    @Test
    public void getAllTransactionsTest() throws IOException, JSONException, URISyntaxException {
        User admin = userServ.saveUser(generateUser("admin@gmail.com"));
        userServ.addRoleToUser("ROLE_ADMIN", admin.getEmail());

        User user1 = userServ.saveUser(generateUser("user1@gmail.com"));
        userServ.addRoleToUser("ROLE_USER", user1.getEmail());
        Account account_user1 = accountServ.saveAccount(new Account("FR7612548029989876543210917", 150.0, "France", user1));
        Card card = cardServ.saveCard(new Card("4012001037141017", "1234", "123", "01/08/2025", false, true, 1500.0, true, false, account_user1, ""));

        User user2 = userServ.saveUser(generateUser("user2@gmail.com"));
        userServ.addRoleToUser("ROLE_USER", user2.getEmail());
        Account account_user2 = accountServ.saveAccount(new Account("FR7612548029989876543210910", 0.0, "France", user2));

        transactionServ.saveTransaction(new Transaction(UUID.randomUUID().toString(), "16/01/2022", "Transaction de 10€", 10.0, 1.0, card, account_user2));
        transactionServ.saveTransaction(new Transaction(UUID.randomUUID().toString(), "16/01/2022", "Transaction de 30€", 30.0, 1.0, card, account_user2));

        String access_token = getToken(admin.getEmail(), "password");

        given()
                .header("Authorization", "Bearer " + access_token)
                .when()
                .get("/operations/transactions")
                .then()
                .statusCode(HttpStatus.SC_OK)
                .and()
                .assertThat()
                .body("_embedded.transactions.size()", equalTo(2));
    }

    @Test
    public void getOneTransactionTest() throws IOException, JSONException, URISyntaxException {
        User admin = userServ.saveUser(generateUser("admin@gmail.com"));
        userServ.addRoleToUser("ROLE_ADMIN", admin.getEmail());

        User user1 = userServ.saveUser(generateUser("user1@gmail.com"));
        userServ.addRoleToUser("ROLE_USER", user1.getEmail());
        Account account_user1 = accountServ.saveAccount(new Account("FR7612548029989876543210917", 150.0, "France", user1));
        Card card = cardServ.saveCard(new Card("4012001037141017", "1234", "123", "01/08/2025", false, true, 1500.0, true, false, account_user1, ""));

        User user2 = userServ.saveUser(generateUser("user2@gmail.com"));
        userServ.addRoleToUser("ROLE_USER", user2.getEmail());
        Account account_user2 = accountServ.saveAccount(new Account("FR7612548029989876543210910", 0.0, "France", user2));

        Transaction transaction = transactionServ.saveTransaction(new Transaction(UUID.randomUUID().toString(), "16/01/2022", "Transaction de 10€", 10.0, 1.0, card, account_user2));

        String access_token = getToken(admin.getEmail(), "password");

        Response response = given()
        .header("Authorization", "Bearer " + access_token)
        .when()
        .get("/operations/transaction/"+transaction.getId())
        .then()
        .statusCode(HttpStatus.SC_OK)
        .extract()
        .response();

        String jsonAsString = response.asString();
        assertThat(jsonAsString, containsString(transaction.getId()));
    }

    @Test
    public void getTransactionsByUserIdTest() throws IOException, JSONException, URISyntaxException {
        User user1 = userServ.saveUser(generateUser("user1@gmail.com"));
        userServ.addRoleToUser("ROLE_USER", user1.getEmail());
        Account account_user1 = accountServ.saveAccount(new Account("FR7612548029989876543210917", 150.0, "France", user1));
        Card card = cardServ.saveCard(new Card("4012001037141017", "1234", "123", "01/08/2025", false, true, 1500.0, true, false, account_user1, ""));

        User user2 = userServ.saveUser(generateUser("user2@gmail.com"));
        userServ.addRoleToUser("ROLE_USER", user2.getEmail());
        Account account_user2 = accountServ.saveAccount(new Account("FR7612548029989876543210910", 0.0, "France", user2));

        transactionServ.saveTransaction(new Transaction(UUID.randomUUID().toString(), "16/01/2022", "Transaction de 10€", 10.0, 1.0, card, account_user2));
        transactionServ.saveTransaction(new Transaction(UUID.randomUUID().toString(), "16/01/2022", "Transaction de 30€", 30.0, 1.0, card, account_user2));
        transactionServ.saveTransaction(new Transaction(UUID.randomUUID().toString(), "16/01/2022", "Transaction de 30€", 5.0, 1.0, card, account_user2));
        transactionServ.saveTransaction(new Transaction(UUID.randomUUID().toString(), "16/01/2022", "Transaction de 30€", 50.0, 1.0, card, account_user2));

        String access_token = getToken(user1.getEmail(), "password");

        given()
        .header("Authorization", "Bearer " + access_token)
        .when()
        .get("/operations/transaction/user/"+user1.getId())
        .then()
        .statusCode(HttpStatus.SC_OK)
        .and()
        .assertThat()
        .body("_embedded.transactionViews.size()", equalTo(4));
    }

    @Test
    public void getTransactionsByUserIdFromWrongUserTest() throws IOException, JSONException, URISyntaxException {

        User wrongUser = userServ.saveUser(generateUser("wrong@gmail.com"));
        userServ.addRoleToUser("ROLE_USER", wrongUser.getEmail());

        User user1 = userServ.saveUser(generateUser("user1@gmail.com"));
        userServ.addRoleToUser("ROLE_USER", user1.getEmail());
        Account account_user1 = accountServ.saveAccount(new Account("FR7612548029989876543210917", 150.0, "France", user1));
        Card card = cardServ.saveCard(new Card("4012001037141017", "1234", "123", "01/08/2025", false, true, 1500.0, true, false, account_user1, ""));

        User user2 = userServ.saveUser(generateUser("user2@gmail.com"));
        userServ.addRoleToUser("ROLE_USER", user2.getEmail());
        Account account_user2 = accountServ.saveAccount(new Account("FR7612548029989876543210910", 0.0, "France", user2));

        transactionServ.saveTransaction(new Transaction(UUID.randomUUID().toString(), "16/01/2022", "Transaction de 10€", 10.0, 1.0, card, account_user2));
        transactionServ.saveTransaction(new Transaction(UUID.randomUUID().toString(), "16/01/2022", "Transaction de 30€", 30.0, 1.0, card, account_user2));
        transactionServ.saveTransaction(new Transaction(UUID.randomUUID().toString(), "16/01/2022", "Transaction de 30€", 5.0, 1.0, card, account_user2));
        transactionServ.saveTransaction(new Transaction(UUID.randomUUID().toString(), "16/01/2022", "Transaction de 30€", 50.0, 1.0, card, account_user2));

        String access_token = getToken(wrongUser.getEmail(), "password");

        given()
        .header("Authorization", "Bearer " + access_token)
        .when()
        .get("/operations/transaction/user/"+user1.getId())
        .then()
        .statusCode(HttpStatus.SC_FORBIDDEN);
    }

    private String toJsonString(Object o) throws JsonProcessingException {
        ObjectMapper map = new ObjectMapper();
        return map.writeValueAsString(o);
    }

    private String getToken(String email, String password) throws JSONException, IOException, URISyntaxException {
        JSONObject json = new JSONObject();
        json.put("email", email);
        json.put("password", password);

        String json_body = String.format("email=%s&password=%s", email, "password");

        Response response = given()
                .with()
                .header("Content-Type", "application/x-www-form-urlencoded")
                .body(json_body)
                .when()
                .post("/login")
                .then()
                .extract()
                .response();

        String stringRes = response.asString();
        JSONObject jsonRes = new JSONObject(stringRes);
        return jsonRes.getString("access_token");
    }

    private User generateUser(String email) {
        return new User(UUID.randomUUID().toString(), "nomUser", "prenomUser", email, "ABCDEFGHIJK125", "0612345682", passwordEncoder.encode("password"), "12/08/1999");
    }
}
